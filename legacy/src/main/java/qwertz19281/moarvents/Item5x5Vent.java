package qwertz19281.moarvents;

import ic2.api.reactor.IReactor;
import ic2.api.reactor.IReactorComponent;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

public class Item5x5Vent extends Item implements IReactorComponent {

	public Item5x5Vent()
	{
		this.setTextureName("moarvents:abzuier5");
		this.setMaxStackSize(64);
		this.setCreativeTab(MoarVents.ctab);
		this.setUnlocalizedName("ventAbzuier");
	}
	
	@Override
	public void processChamber(IReactor reactor, ItemStack yourStack, int x, int y, boolean heatrun) {

	}

	@Override
	public boolean acceptUraniumPulse(IReactor reactor, ItemStack yourStack, ItemStack pulsingStack, int youX, int youY,
			int pulseX, int pulseY, boolean heatrun) {
		return false;
	}

	@Override
	public boolean canStoreHeat(IReactor reactor, ItemStack yourStack, int x, int y) {
		return true;
	}

	@Override
	public int getMaxHeat(IReactor reactor, ItemStack yourStack, int x, int y) {
		return Integer.MAX_VALUE;
	}

	@Override
	public int getCurrentHeat(IReactor reactor, ItemStack yourStack, int x, int y) {
		return 0;
	}

	@Override
	public int alterHeat(IReactor reactor, ItemStack yourStack, int x, int y, int heat) {
		int haet=heat;
		if(haet<1){return haet;}
		if(reactor.isFluidCooled()){reactor.addEmitHeat(haet);}else{reactor.addHeat(haet);}
		return 0;
	}

	@Override
	public float influenceExplosion(IReactor reactor, ItemStack yourStack) {
		return 0;
	}

}
