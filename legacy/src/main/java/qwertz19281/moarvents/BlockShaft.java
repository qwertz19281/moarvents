package qwertz19281.moarvents;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

public class BlockShaft extends Block implements ITileEntityProvider
{
	public BlockShaft()
	{
		this(Material.rock);
	}

	public BlockShaft(Material mat)
	{
		super(mat);
		this.setBlockName("util.shaft");
		this.setBlockTextureName("moarvents:shaft");
		this.setCreativeTab(MoarVents.ctab);
	}

	/**
	 * Returns a new instance of a block's tile entity class. Called on placing the block.
	 */
	public TileEntity createNewTileEntity(World wld, int mat)
	{
		return new TileEntityShaft();
	}
	
	@Override
	public int getFlammability(IBlockAccess world, int x, int y, int z, ForgeDirection face)
	{
		return 0;
	}
}
