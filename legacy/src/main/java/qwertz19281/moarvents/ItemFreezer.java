package qwertz19281.moarvents;

import ic2.api.reactor.IReactor;
import ic2.api.reactor.IReactorComponent;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

public class ItemFreezer extends Item implements IReactorComponent {

	public ItemFreezer()
	{
		this.setTextureName("moarvents:abzuierB5");
		this.setMaxStackSize(64);
		this.setCreativeTab(MoarVents.ctab);
		this.setUnlocalizedName("ventFreezer");
	}
	
	@Override
	public void processChamber(IReactor reactor, ItemStack yourStack, int x, int y, boolean heatrun) {
		if(heatrun){
			int toMove=Math.min(128,reactor.getHeat());
			int heat=toMove;
			//System.out.print(heat+" ");
			heat=alterNeighborHeat(reactor,x+1,y,heat);
			//System.out.print(heat+" ");
			heat=alterNeighborHeat(reactor,x,y+1,heat);
			//System.out.print(heat+" ");
			heat=alterNeighborHeat(reactor,x-1,y,heat);
			//System.out.print(heat+" ");
			heat=alterNeighborHeat(reactor,x,y-1,heat);
			//System.out.print(heat+"\n");
			reactor.addHeat(heat-toMove);
		}
	}

	@Override
	public boolean acceptUraniumPulse(IReactor reactor, ItemStack yourStack, ItemStack pulsingStack, int youX, int youY,
			int pulseX, int pulseY, boolean heatrun) {
		return false;
	}

	@Override
	public boolean canStoreHeat(IReactor reactor, ItemStack yourStack, int x, int y) {
		return false;
	}

	@Override
	public int getMaxHeat(IReactor reactor, ItemStack yourStack, int x, int y) {
		return 0;
	}

	@Override
	public int getCurrentHeat(IReactor reactor, ItemStack yourStack, int x, int y) {
		return 0;
	}

	@Override
	public int alterHeat(IReactor reactor, ItemStack yourStack, int x, int y, int heat) {
		/*int haet=heat;
		if(haet>=0){return haet;}
		haet=Math.max(haet,-reactor.getHeat());
		haet=Math.max(haet,);
		reactor.addHeat(-haet);
		return haet+heat;*/
		return heat;
	}

	@Override
	public float influenceExplosion(IReactor reactor, ItemStack yourStack) {
		return 0;
	}

	public int alterNeighborHeat(IReactor reactor,int x,int y,int heat){
		ItemStack is=reactor.getItemAt(x, y);
		if(is==null){return heat;}
		return ((IReactorComponent)is.getItem()).alterHeat(reactor, reactor.getItemAt(x, y),x,y,heat);
	}
}
