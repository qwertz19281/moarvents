package qwertz19281.moarvents;

import ic2.api.energy.tile.IHeatSource;
import ic2.api.reactor.IReactor;
import ic2.api.reactor.IReactorChamber;
import ic2.core.block.reactor.tileentity.TileEntityReactorAccessHatch;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.ForgeDirection;

public class TileEntityRHB extends TileEntity implements IHeatSource
{
	int ticker=0,datmax=0,dattransed=0;double hi;
	boolean inSec;
	
	public void updateEntity() {
		ticker++;
		if(ticker==20){
			ticker=0;
			inSec=false;
			searchForHeatAndInjectIntoReactor(true);
		} else {
			searchForHeatAndInjectIntoReactor(false);
		}
	}
	
	public boolean canUpdate()
	{
		return true;
	}
	
	public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        ticker=tag.getInteger("tickah");
        datmax=tag.getInteger("datmax");
        dattransed=tag.getInteger("dattransed");
        inSec=tag.getBoolean("inSec");
        hi=tag.getDouble("hi");
    }

    public void writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        tag.setInteger("tickah", ticker);
        tag.setInteger("datmax", datmax);
        tag.setInteger("dattransed", dattransed);
        tag.setBoolean("inSec", inSec);
        tag.setDouble("hi", hi);
    }

    public void searchForHeatAndInjectIntoReactor(boolean inject){
    	int heattopull=256,heatpulled=0;
    	IHeatSource sd=getHeatSourceNotSelf(worldObj.getTileEntity(xCoord, yCoord-1, zCoord));
		IHeatSource su=getHeatSourceNotSelf(worldObj.getTileEntity(xCoord, yCoord+1, zCoord));
		IHeatSource se=getHeatSourceNotSelf(worldObj.getTileEntity(xCoord+1, yCoord, zCoord));
		IHeatSource ss=getHeatSourceNotSelf(worldObj.getTileEntity(xCoord, yCoord, zCoord+1));
		IHeatSource sw=getHeatSourceNotSelf(worldObj.getTileEntity(xCoord-1, yCoord, zCoord));
		IHeatSource sn=getHeatSourceNotSelf(worldObj.getTileEntity(xCoord, yCoord, zCoord-1));
		IReactor d=searchForInterface(worldObj.getTileEntity(xCoord, yCoord-1, zCoord));
		IReactor u=searchForInterface(worldObj.getTileEntity(xCoord, yCoord+1, zCoord));
		IReactor e=searchForInterface(worldObj.getTileEntity(xCoord+1, yCoord, zCoord));
		IReactor s=searchForInterface(worldObj.getTileEntity(xCoord, yCoord, zCoord+1));
		IReactor w=searchForInterface(worldObj.getTileEntity(xCoord-1, yCoord, zCoord));
		IReactor n=searchForInterface(worldObj.getTileEntity(xCoord, yCoord, zCoord-1));
		int count=0;
		if(d!=null){count++;}
		if(u!=null){count++;}
		if(e!=null){count++;}
		if(s!=null){count++;}
		if(w!=null){count++;}
		if(n!=null){count++;}
		if(count==0){return;}
		//NOW PULL ALL HEAT WHICH IS PULLABLE
		if(sd!=null){hi+=sd.requestHeat(ForgeDirection.UP, Math.min(sd.maxrequestHeatTick(ForgeDirection.UP),(int)(4096-hi)));}
		if(su!=null){hi+=su.requestHeat(ForgeDirection.DOWN, Math.min(su.maxrequestHeatTick(ForgeDirection.DOWN),(int)(4096-hi)));}
		if(se!=null){hi+=se.requestHeat(ForgeDirection.WEST, Math.min(se.maxrequestHeatTick(ForgeDirection.WEST),(int)(4096-hi)));}
		if(ss!=null){hi+=ss.requestHeat(ForgeDirection.NORTH, Math.min(ss.maxrequestHeatTick(ForgeDirection.NORTH),(int)(4096-hi)));}
		if(sw!=null){hi+=sw.requestHeat(ForgeDirection.EAST, Math.min(sw.maxrequestHeatTick(ForgeDirection.EAST),(int)(4096-hi)));}
		if(sn!=null){hi+=sn.requestHeat(ForgeDirection.SOUTH, Math.min(sn.maxrequestHeatTick(ForgeDirection.SOUTH),(int)(4096-hi)));}
		if(!inject){return;}
		int totransfer=(int)(hi/count/40);
		hi-=totransfer*count*40;
		if(d!=null){d.addHeat(totransfer);}
		if(u!=null){u.addHeat(totransfer);}
		if(e!=null){e.addHeat(totransfer);}
		if(s!=null){s.addHeat(totransfer);}
		if(w!=null){w.addHeat(totransfer);}
		if(n!=null){n.addHeat(totransfer);}
    }
    
	@Override
	public int maxrequestHeatTick(ForgeDirection directionFrom) {
		// TODO Auto-generated method stub
		IReactor d=searchForInterface(worldObj.getTileEntity(xCoord, yCoord-1, zCoord));
		IReactor u=searchForInterface(worldObj.getTileEntity(xCoord, yCoord+1, zCoord));
		IReactor e=searchForInterface(worldObj.getTileEntity(xCoord+1, yCoord, zCoord));
		IReactor s=searchForInterface(worldObj.getTileEntity(xCoord, yCoord, zCoord+1));
		IReactor w=searchForInterface(worldObj.getTileEntity(xCoord-1, yCoord, zCoord));
		IReactor n=searchForInterface(worldObj.getTileEntity(xCoord, yCoord, zCoord-1));
		/*int count=0;
		if(d!=null){count++;}
		if(u!=null){count++;}
		if(e!=null){count++;}
		if(s!=null){count++;}
		if(w!=null){count++;}
		if(n!=null){count++;}*/
		int max=0;
		if(d!=null){max+=d.getHeat();}
		if(u!=null){max+=u.getHeat();}
		if(e!=null){max+=e.getHeat();}
		if(s!=null){max+=s.getHeat();}
		if(w!=null){max+=w.getHeat();}
		if(n!=null){max+=n.getHeat();}
		//return max*2;
		//System.out.print("REQ"+max);
		if(max==0){
			if(inSec){
				//System.out.println(" "+max*2+" T"+ticker+" IS"+inSec);
				return datmax;
			} else {
				//System.out.println(" 0 T"+ticker+" IS"+inSec);
				return 0;
			}
		} else {
			inSec=true;
			ticker=0;
			datmax=max*2;
			//System.out.println(" "+max*2+" T"+ticker+" IS"+inSec);
			return max*2;
		}
	}

	@Override
	public int requestHeat(ForgeDirection directionFrom, int requestheat) {
		// TODO Auto-generated method stub
		if(requestheat<=0){return 0;}
		IReactor d=searchForInterface(worldObj.getTileEntity(xCoord, yCoord-1, zCoord));
		IReactor u=searchForInterface(worldObj.getTileEntity(xCoord, yCoord+1, zCoord));
		IReactor e=searchForInterface(worldObj.getTileEntity(xCoord+1, yCoord, zCoord));
		IReactor s=searchForInterface(worldObj.getTileEntity(xCoord, yCoord, zCoord+1));
		IReactor w=searchForInterface(worldObj.getTileEntity(xCoord-1, yCoord, zCoord));
		IReactor n=searchForInterface(worldObj.getTileEntity(xCoord, yCoord, zCoord-1));
		int count=0,available=0;
		if(d!=null){count++;available+=d.getHeat();}
		if(u!=null){count++;available+=u.getHeat();}
		if(e!=null){count++;available+=e.getHeat();}
		if(s!=null){count++;available+=s.getHeat();}
		if(w!=null){count++;available+=w.getHeat();}
		if(n!=null){count++;available+=n.getHeat();}
		int trans=Math.min(requestheat/2,available);
		int subtrans=trans/count; // ROUND ROBIN
		int transed=0;
		if(d!=null){
			int tomove=Math.min(subtrans,d.getHeat());
			d.addHeat(-tomove);
			transed+=tomove;
		}
		if(u!=null){
			int tomove=Math.min(subtrans,u.getHeat());
			u.addHeat(-tomove);
			transed+=tomove;
		}
		if(e!=null){
			int tomove=Math.min(subtrans,e.getHeat());
			e.addHeat(-tomove);
			transed+=tomove;
		}
		if(s!=null){
			int tomove=Math.min(subtrans,s.getHeat());
			s.addHeat(-tomove);
			transed+=tomove;
		}
		if(w!=null){
			int tomove=Math.min(subtrans,w.getHeat());
			w.addHeat(-tomove);
			transed+=tomove;
		}
		if(n!=null){
			int tomove=Math.min(subtrans,n.getHeat());
			n.addHeat(-tomove);
			transed+=tomove;
		}
		//System.out.print("DO"+transed);
		/*if(transed==0){
			if(inSec){
				//System.out.println(" "+transed*2+" T"+ticker+" IS"+inSec);
				return dattransed;
			} else {
				//System.out.println(" 0 T"+ticker+" IS"+inSec);
				return 0;
			}
		} else {
			inSec=true;
			ticker=0;
			dattransed=transed*2;
			//System.out.println(" "+transed*2+" T"+ticker+" IS"+inSec);
			return transed*2;
		}*/
		return transed*40;
		//return transed*2;
	}
	
	public IReactor searchForInterface(TileEntity te){
		if(te instanceof IReactor){return (IReactor)te;}
		else if(te instanceof IReactorChamber)
		{return (IReactor)((IReactorChamber)te).getReactor();}
		else if(te instanceof TileEntityReactorAccessHatch)
		{return (IReactor)((TileEntityReactorAccessHatch)te).getReactor();}//RLY?? You don't need this to cool an fluid reactor!!
		return null;
	}
	public IHeatSource getHeatSourceNotSelf(TileEntity te){
		if(te instanceof TileEntityRHB){return null;}
		if(te instanceof IHeatSource){return (IHeatSource)te;}
		return null;
	}
}
