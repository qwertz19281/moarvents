package qwertz19281.moarvents;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import ic2.core.Ic2Items;

import java.util.Calendar;
import java.util.Random;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.oredict.ShapedOreRecipe;

@Mod(
		modid = "moarvents",
		name = "MoarVents",
		version = "$(VERSION)"
		)
public class MoarVents
{
	public static CRETAB ctab = new CRETAB("moarvents");

	static class CRETAB extends CreativeTabs {
		public CRETAB(String lable) {
			super(lable);
		}
		Item tabitem=new Item().setUnlocalizedName("mv_ctab_tex_item").setTextureName("moarvents:abzuierB4");
		public Item getTabIconItem()
		{
			return tabitem;
		}
	}
	public static int lcnt = 9;
	public static boolean selfSave = false;
	public static boolean createEmitHeat = true;
	public static boolean advLog = true;
	public static Random rnd = new Random();
	public static BlockShaft shaft = new BlockShaft();
	public static BlockHeatConducter cndctr = new BlockHeatConducter(Material.rock);
	public static BlockRHB rhb = new BlockRHB(Material.rock);
	//public static BlockMaxKabel maxkabel = new BlockMaxKabel(Material.cloth);
	public static Item5x5Vent[] va = new Item5x5Vent[] {new Item5x5Vent(), new Item5x5Vent(), new Item5x5Vent(), new Item5x5Vent(), new Item5x5Vent()};
	public static ItemFreezer[] vaB = new ItemFreezer[] {new ItemFreezer(), new ItemFreezer(), new ItemFreezer(), new ItemFreezer(), new ItemFreezer()};
	public static Item[] mems = new Item[] {(new Item()).setUnlocalizedName("heatBuffer").setTextureName("moarvents:cry").setCreativeTab(CreativeTabs.tabMisc), (new Item()).setUnlocalizedName("membranComp").setTextureName("moarvents:membc").setCreativeTab(CreativeTabs.tabMisc), (new Item()).setUnlocalizedName("membranLiqHeat").setTextureName("moarvents:membf").setCreativeTab(CreativeTabs.tabMisc), (new Item()).setUnlocalizedName("hmembranTemp").setTextureName("moarvents:membh").setCreativeTab(CreativeTabs.tabMisc)};
	public static ItemOWHR owhr=new ItemOWHR();public static ItemOWHD owhd=new ItemOWHD();public static ItemOWHL owhl=new ItemOWHL();public static ItemOWHU owhu=new ItemOWHU();
	public static ItemColdTasche coldt=new ItemColdTasche();
	public static ItemHotTasche hott=new ItemHotTasche();

	/*@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		Calendar calendar = Calendar.getInstance();

		if (calendar.get(2) + 1 == 12 && calendar.get(5) >= 24 && calendar.get(5) <= 26)
		{
			event.getModMetadata().logoFile = "/assets/moarvents/textures/logoxmas.png";
		}
		else if (calendar.get(2) == 3 && calendar.get(5) == 1)
		{
			event.getModMetadata().logoFile = "/assets/moarvents/textures/logo0.png";
		}
		else
		{
			int config = rnd.nextInt(lcnt);

			if (config < 1)
			{
				config = 1;
			}

			if (config > lcnt)
			{
				config = lcnt;
			}

			event.getModMetadata().logoFile = "/assets/moarvents/textures/logo" + String.valueOf(config) + ".png";
		}

		try
		{
			Class var10000 = MoarVents.class;
			Class.forName("net.minecraftforge.common.config.Configuration");
		}
		catch (ClassNotFoundException var4)
		{
			System.out.println("WARNING! You need Minecraft Forge, not only FML, to use the config system, else only the default configs will be used");
			return;
		}

		System.out.println("[MoarVents] try load config");
		Configuration config1 = null;
		config1 = new Configuration(event.getSuggestedConfigurationFile());
		config1.load();
		selfSave = config1.get("ADVANCED - Changing at your own risk!", "selfSave", false, "(Changing at your own risk!) save component heat self, too. Default: false").getBoolean();
		createEmitHeat = config1.get("ADVANCED - Changing at your own risk!", "createEmitHeat", true, "(Changing at your own risk!) generate hU. Default: true").getBoolean();
		advLog = config1.get("General", "AdvancedLogging", true, "- Enable or disable Advanced Logging. Default: true").getBoolean();
		config1.save();
		System.out.println("[MoarVents] config job sucessfull");
	}*/

	public static boolean foolDay=false;

	@EventHandler
	public void load(FMLInitializationEvent event)
	{	
		try{
			Calendar calendar = Calendar.getInstance();
			if (calendar.get(calendar.MONTH) == 3 && calendar.get(calendar.DAY_OF_MONTH) == 1) {
				foolDay=true;
			}
			calendar=null;
			if(foolDay){
				try{
					throw new ClassNotFoundException("qwertz19281.moarvents.AprilFool");
				}catch(ClassNotFoundException ex){
					ex.printStackTrace();
				}
			}
		}catch(Exception ex){}
		GameRegistry.registerItem(ctab.tabitem, "mv_ctab_placeholder");
		LanguageRegistry.addName(ctab.tabitem, "Useless CreativeTab Placeholder");
		int bc;

		//for (bc = 0; bc < va.length; ++bc)
		//{
		va[4].setCreativeTab(ctab);
		//}

		//for (bc = 0; bc < vaB.length; ++bc)
		//{
		vaB[4].setCreativeTab(ctab);
		//}

		for (bc = 0; bc < mems.length; ++bc)
		{
			mems[bc].setCreativeTab(ctab);
		}

		GameRegistry.registerItem(va[0], "VentAbzuierA");
		GameRegistry.registerItem(va[1], "VentAbzuierB");
		GameRegistry.registerItem(va[2], "VentAbzuierC");
		GameRegistry.registerItem(va[3], "VentAbzuierD");
		GameRegistry.registerItem(va[4], "VentAbzuierE");
		LanguageRegistry.addName(va[0], "5x5 Reactor Liquid Pipe");
		LanguageRegistry.addName(va[1], "5x5 Reactor Liquid Pipe");
		LanguageRegistry.addName(va[2], "5x5 Reactor Liquid Pipe");
		LanguageRegistry.addName(va[3], "5x5 Reactor Liquid Pipe");
		LanguageRegistry.addName(va[4], "5x5 Reactor Liquid Pipe");
		GameRegistry.registerItem(vaB[0], "VentFreezerA");
		GameRegistry.registerItem(vaB[1], "VentFreezerB");
		GameRegistry.registerItem(vaB[2], "VentFreezerC");
		GameRegistry.registerItem(vaB[3], "VentFreezerD");
		GameRegistry.registerItem(vaB[4], "VentFreezerE");
		LanguageRegistry.addName(vaB[0], "Reactor Cooldown");
		LanguageRegistry.addName(vaB[1], "Reactor Cooldown");
		LanguageRegistry.addName(vaB[2], "Reactor Cooldown");
		LanguageRegistry.addName(vaB[3], "Reactor Cooldown");
		LanguageRegistry.addName(vaB[4], "Reactor Cooldown");
		LanguageRegistry.instance().addStringLocalization("itemGroup.moarvents", "MoarVents");
		GameRegistry.registerItem(mems[0], "VentBuffer");
		GameRegistry.registerItem(mems[1], "MembComp");
		GameRegistry.registerItem(mems[2], "MembHU");
		GameRegistry.registerItem(mems[3], "MembTemp");
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(mems[0], 4), new Object[] {Boolean.valueOf(false), new Object[]{"mwm", "wdw", "mwm", 'm', Items.iron_ingot, 'w', Items.water_bucket, 'd', "dustCopper"}}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(mems[1], 4), new Object[] {Boolean.valueOf(false), new Object[]{"m  ", " m ", " rm", 'm', Items.iron_ingot, 'r', "itemRubber"}}));
		GameRegistry.addShapedRecipe(new ItemStack(mems[2], 4), new Object[] {"msm", " m ", "   ", 'm', Items.iron_ingot, 's', Blocks.sponge});
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(mems[2], 4), new Object[] {Boolean.valueOf(false), new Object[]{"msm", " s ", " m ", 'm', Items.iron_ingot, 's', "dustStone"}}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(mems[3], 4), new Object[] {Boolean.valueOf(false), new Object[]{"mrm", " w ", "   ", 'm', Items.iron_ingot, 'w', Items.water_bucket, 'r', "itemRubber"}}));
		GameRegistry.addShapedRecipe(new ItemStack(va[0], 4), new Object[] {"i i", "cb ", "ifi", 'i', Items.iron_ingot, 'b', mems[0], 'c', mems[1], 'f', mems[2]});

		/*for (bc = 1; bc < va.length; ++bc)
		{
			if (bc != 2 && bc != 4)
			{
				GameRegistry.addShapedRecipe(new ItemStack(va[bc], 4), new Object[] {" vv", "cvv", " f ", 'v', va[bc - 1], 'c', mems[1], 'f', mems[2]});
			}
			else
			{
				GameRegistry.addShapedRecipe(new ItemStack(va[bc], 4), new Object[] {" vv", "cvv", "bf ", 'v', va[bc - 1], 'c', mems[1], 'f', mems[2], 'b', mems[0]});
			}
		}*/

		GameRegistry.addShapedRecipe(new ItemStack(vaB[0], 4), new Object[] {"ihi", " bc", "i i", 'i', Items.iron_ingot, 'h', mems[3], 'c', mems[1], 'b', mems[0]});

		/*for (bc = 1; bc < vaB.length; ++bc)
		{
			if (bc != 2 && bc != 4)
			{
				GameRegistry.addShapedRecipe(new ItemStack(vaB[bc], 4), new Object[] {" h ", "vvc", "vv ", 'v', vaB[bc - 1], 'c', mems[1], 'h', mems[3]});
			}
			else
			{
				GameRegistry.addShapedRecipe(new ItemStack(vaB[bc], 4), new Object[] {" hb", "vvc", "vv ", 'v', vaB[bc - 1], 'c', mems[1], 'h', mems[3], 'b', mems[0]});
			}
		}*/

		LanguageRegistry.addName(mems[0], "Vent Heat Buffer");
		LanguageRegistry.addName(mems[1], "Component Membran");
		LanguageRegistry.addName(mems[2], "Dissipation Membran");
		LanguageRegistry.addName(mems[3], "CoreTemp cooling membran");
		TileEntity.addMapping(TileEntityShaft.class, "mv_shaft");
		TileEntity.addMapping(TileEntityHeatConducter.class, "mv_heco");
		TileEntity.addMapping(TileEntityRHB.class, "moarvents.rhb");
		//TileEntity.addMapping(TEMaxKabel.class, "mv_maxkabel");
		//this.log("abzuied");
		GameRegistry.registerBlock(cndctr, "HeatCndctr");
		LanguageRegistry.addName(cndctr, "Heat Conducter (VERY BUGGY!!!! DONT BUILD CIRCLES!!! (STACKOVERFLOW))");
		//GameRegistry.addShapedRecipe(new ItemStack(cndctr, 8), new Object[] {"a  ", "   ", "   ", 'a', mems[1]});
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(cndctr, 1), new Object[] {Boolean.valueOf(false), new Object[]{"ccc", "sss", "ccc", 'c', "ingotCopper", 's', "ingotSilver"}}));
		GameRegistry.registerBlock(rhb, "RHB");
		LanguageRegistry.addName(rhb, "Reactor Heat Bridge");
		//GameRegistry.addShapedRecipe(new ItemStack(rhb, 8), new Object[] {"  a", "   ", "   ", 'a', mems[1]});
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(rhb, 1), new Object[] {Boolean.valueOf(false), new Object[]{"ccc", "msm", "ccc", 'm', mems[1], 'c', "ingotCopper", 's', "ingotSilver"}}));
		GameRegistry.registerBlock(shaft, "KinShft");
		LanguageRegistry.addName(shaft, "Kinetic Shaft");
		//GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(shaft, 4), new Object[] {Boolean.valueOf(false), new Object[]{" h ", "sss", " h ", 'h', Blocks.stone, 's', Ic2Items.ironshaft, 'd', "dustCopper"}}));
		//GameRegistry.addShapedRecipe(new ItemStack(shaft, 8), new Object[] {" a ", "   ", "   ", 'a', mems[1]});
		GameRegistry.addShapedRecipe(new ItemStack(shaft, 8), new Object[] {" h ", "sss", " h ", 'h', Blocks.stone, 's', Ic2Items.ironshaft});
		//GameRegistry.registerBlock(maxkabel, "maxkabel");
		//LanguageRegistry.instance().addStringLocalization("tile.maxkabel", "de_DE", "MAX KABEL");
		//LanguageRegistry.addName(maxkabel, "MAX CABLE");
		//GameRegistry.addShapedRecipe(new ItemStack(maxkabel, 8), new Object[] {"  a", "   ", "   ", 'a', mems[1]});
		GameRegistry.registerItem(owhr, "OHWR");
		GameRegistry.registerItem(owhd, "OHWD");
		GameRegistry.registerItem(owhl, "OHWL");
		GameRegistry.registerItem(owhu, "OHWU");
		GameRegistry.addShapedRecipe(new ItemStack(owhr, 4), new Object[] {"   ", "  c", "   ", 'c', mems[1]});
		GameRegistry.addShapedRecipe(new ItemStack(owhd, 4), new Object[] {"   ", "   ", " c ", 'c', mems[1]});
		GameRegistry.addShapedRecipe(new ItemStack(owhl, 4), new Object[] {"   ", "c  ", "   ", 'c', mems[1]});
		GameRegistry.addShapedRecipe(new ItemStack(owhu, 4), new Object[] {" c ", "   ", "   ", 'c', mems[1]});
		LanguageRegistry.addName(owhr, "One Way Heat (R)");
		LanguageRegistry.addName(owhd, "One Way Heat (D)");
		LanguageRegistry.addName(owhl, "One Way Heat (L)");
		LanguageRegistry.addName(owhu, "One Way Heat (U)");
		GameRegistry.registerItem(coldt, "heatCold");
		GameRegistry.registerItem(hott, "heatHot");
		GameRegistry.addShapedRecipe(new ItemStack(coldt, 8), new Object[] {"mmm", "mim", "mmm", 'm', mems[0], 'i', Items.iron_ingot});
		LanguageRegistry.addName(coldt, "Heat Storage");
		LanguageRegistry.addName(hott, "Heat Storage (Hot)");
		//GameRegistry.regi
	}

	/*public void log(String texa)
	{
		if (advLog)
		{
			if (!texa.startsWith("[MoarVents]"))
			{
				System.out.println("[MoarVents] " + texa);
			}
			else
			{
				System.out.println(texa);
			}
		}
	}*/
}
