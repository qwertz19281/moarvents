package qwertz19281.moarvents;

import java.util.ArrayList;

import ic2.api.reactor.IReactor;
import ic2.api.reactor.IReactorComponent;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

public class ItemOWHR extends Item implements IReactorComponent,IAlderHeat {

	public ItemOWHR()
	{
		this.setTextureName("moarvents:arrowR");
		this.setMaxStackSize(1);
		this.setCreativeTab(MoarVents.ctab);
		this.setUnlocalizedName("ventOWHR");
	}

	@Override
	public void processChamber(IReactor reactor, ItemStack yourStack, int x, int y, boolean heatrun) {

	}

	@Override
	public boolean acceptUraniumPulse(IReactor reactor, ItemStack yourStack, ItemStack pulsingStack, int youX, int youY,
			int pulseX, int pulseY, boolean heatrun) {
		return false;
	}

	@Override
	public boolean canStoreHeat(IReactor reactor, ItemStack yourStack, int x, int y) {
		return false;
	}

	@Override
	public int getMaxHeat(IReactor reactor, ItemStack yourStack, int x, int y) {
		return 0;
	}

	@Override
	public int getCurrentHeat(IReactor reactor, ItemStack yourStack, int x, int y) {
		return 0;
	}

	boolean evilbaum=true;


	@Override
	public int alterHeat(IReactor reactor, ItemStack yourStack, int x, int y, int heat) {
		int haet=heat;
		if(haet<1){return haet;}
		ArrayList<IAlderHeat> arhc=new ArrayList<IAlderHeat>();
		arhc.add(this);
		haet=alterNeighborHeat(reactor,x+1,y,haet,arhc);
		arhc.clear();
		return haet;
	}

	public int alderHeat(IReactor reactor, ItemStack yourStack, int x, int y, int heat, ArrayList<IAlderHeat> arhc) {
		int haet=heat;
		if(arhc.contains(this)){if(evilbaum){System.err.println("[MoarVents] at 37 OWHD");evilbaum=false;}return haet;}
		arhc.add(this);
		if(haet<1){return haet;}
		haet=alterNeighborHeat(reactor,x+1,y,haet,arhc);
		return haet;
	}

	public int alterNeighborHeat(IReactor reactor,int x,int y,int heat,ArrayList<IAlderHeat> arhc){
		ItemStack is=reactor.getItemAt(x, y);
		if(is==null){return heat;}
		IReactorComponent rc=((IReactorComponent)is.getItem());
		if(rc instanceof IAlderHeat){
			return ((IAlderHeat)rc).alderHeat(reactor, reactor.getItemAt(x, y), x, y, heat,arhc);
		} else {	
			return rc.alterHeat(reactor, reactor.getItemAt(x,y), x,y, heat);
		}
	}

	@Override
	public float influenceExplosion(IReactor reactor, ItemStack yourStack) {
		return 0;
	}
}
