package qwertz19281.moarvents;

import java.util.ArrayList;

import ic2.api.energy.tile.IHeatSource;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.ForgeDirection;

public class TileEntityHeatConducter extends TileEntity implements IHeatSource
{
	public int maxrequestHeatTick(ForgeDirection directionFrom)
	{
		int voa = 0;

		ArrayList<TileEntityHeatConducter> begin=new ArrayList<TileEntityHeatConducter>();
		begin.add(this);
		
		for (int ax = 0; ax < 6; ++ax)
		{
			ForgeDirection fgd = ForgeDirection.VALID_DIRECTIONS[ax];

			if (!fgd.equals(directionFrom))
			{
				TileEntity te = this.worldObj.getTileEntity(xCoord + fgd.offsetX, yCoord + fgd.offsetY, zCoord + fgd.offsetZ);

				if (te != null)
				{
					if (te instanceof TileEntityHeatConducter)
					{
						voa += ((TileEntityHeatConducter)te).maxrequestHeatTick(fgd.getOpposite(), begin);
					}
					else if (te instanceof IHeatSource)
					{
						voa += ((IHeatSource)te).maxrequestHeatTick(fgd.getOpposite());
					}
				}
			}
		}

		begin.clear();
		
		return voa;
	}

	public int maxrequestHeatTick(ForgeDirection directionFrom, ArrayList<TileEntityHeatConducter> arhc)
	{
		if (arhc.contains(this))
		{
			return 0;
		}
		else
		{
			arhc.add(this);
			int voa = 0;

			for (int ax = 0; ax < 6; ++ax)
			{
				ForgeDirection fgd = ForgeDirection.VALID_DIRECTIONS[ax];

				if (!fgd.equals(directionFrom))
				{
					TileEntity te = this.worldObj.getTileEntity(xCoord + fgd.offsetX, yCoord + fgd.offsetY, zCoord + fgd.offsetZ);

					if (te != null)
					{
						if (te instanceof TileEntityHeatConducter)
						{
							voa += ((TileEntityHeatConducter)te).maxrequestHeatTick(fgd.getOpposite(), arhc);
						}
						else if (te instanceof IHeatSource)
						{
							voa += ((IHeatSource)te).maxrequestHeatTick(fgd.getOpposite());
						}
					}
				}
			}

			return voa;
		}
	}

	public int requestHeat(ForgeDirection directionFrom, int requestheat)
	{
		int voa = requestheat;

		ArrayList<TileEntityHeatConducter> begin=new ArrayList<TileEntityHeatConducter>();
		begin.add(this);
		
		for (int ax = 0; ax < 6; ++ax)
		{
			ForgeDirection fgd = ForgeDirection.VALID_DIRECTIONS[ax];

			if (!fgd.equals(directionFrom))
			{
				TileEntity te = this.worldObj.getTileEntity(xCoord + fgd.offsetX, yCoord + fgd.offsetY, zCoord + fgd.offsetZ);

				if (te != null)
				{
					if (te instanceof TileEntityHeatConducter)
					{
						voa -= ((TileEntityHeatConducter)te).requestHeat(fgd.getOpposite(), voa, begin);
					}
					else if (te instanceof IHeatSource)
					{
						voa -= ((IHeatSource)te).requestHeat(fgd.getOpposite(), voa);
					}
				}
			}
		}

		begin.clear();
		
		return requestheat - voa;
	}

	public int requestHeat(ForgeDirection directionFrom, int requestheat, ArrayList<TileEntityHeatConducter> arhc)
	{
		if (arhc.contains(this))
		{
			return 0;
		}
		else
		{
			arhc.add(this);
			int voa = requestheat;

			for (int ax = 0; ax < 6; ++ax)
			{
				ForgeDirection fgd = ForgeDirection.VALID_DIRECTIONS[ax];

				if (!fgd.equals(directionFrom))
				{
					TileEntity te = this.worldObj.getTileEntity(xCoord + fgd.offsetX, yCoord + fgd.offsetY, zCoord + fgd.offsetZ);

					if (te != null)
					{
						if (te instanceof TileEntityHeatConducter)
						{
							voa -= ((TileEntityHeatConducter)te).requestHeat(fgd.getOpposite(), voa, arhc);
						}
						else if (te instanceof IHeatSource)
						{
							voa -= ((IHeatSource)te).requestHeat(fgd.getOpposite(), voa);
						}
					}
				}
			}

			return requestheat - voa;
		}
	}

	public boolean canUpdate()
	{
		return false;
	}
}
