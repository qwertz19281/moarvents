# MoarVents #

![ScreenshotMV.png](https://sites.google.com/site/moarvents/home/ScreenshotMV.png)

[All items](https://sites.google.com/site/moarvents/home/items)

# [DOWNLOADS (1.12.2)](https://bitbucket.org/qwertz19281/moarvents/src/master/1.12.2/build/libs/) # beta1.2.5.2
# [DOWNLOADS (1.10.2)](https://bitbucket.org/qwertz19281/moarvents/src/master/1.10.2/build/libs/) # beta1.2.5.2
1.10.2 IMPORTANT: 1.2.5 changes item IDs all items are lost and TURN OFF YOUR REACTORS BEFORE UPGRADING

# [(legacy) DOWNLOADS (1.7.10)](https://bitbucket.org/qwertz19281/moarvents/src/master/1.7.10/build/libs/) # beta1.2.4

[License](https://bitbucket.org/qwertz19281/moarvents/raw/master/MoarVents_License.txt)
![    ](https://bitbucket.org/qwertz19281/beama/downloads/moarvents.png)
