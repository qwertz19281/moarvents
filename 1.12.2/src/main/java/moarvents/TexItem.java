package moarvents;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;

public class TexItem extends Item {
	
	public TexItem() {
		super();
	}
	
	public TexItem setTextureName() {
		if(FMLCommonHandler.instance().getSide()==Side.CLIENT){
			String texName="moarvents:"+(getUnlocalizedName().substring(5));
			System.out.println("Load Item tex: "+texName);
			Minecraft
                .getMinecraft()
                .getRenderItem()
                .getItemModelMesher()
                .register(this, 0,
                        new ModelResourceLocation(texName, "inventory"));
        }
		return this;
	}
	
	public void doTexReg() {
		
	}
	
	public TexItem setUnlocalizedNameB(String unlocalizedName) {
		setRegistryName(unlocalizedName);
		return (TexItem)setUnlocalizedName(unlocalizedName);
    }
}
