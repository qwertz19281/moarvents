package moarvents;

import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockRdsw extends TexBlock implements ITileEntityProvider
{
	public BlockRdsw(Material m)
	{
		super(m);
		this.setUnlocalizedName("rdsw");
		this.setRegistryName("rdsw");
		//this.setBlockTextureName();
		this.setCreativeTab(MoarVents.ctab);
	}

	/**
	 * Returns a new instance of a block's tile entity class. Called on placing the block.
	 */
	@Override
	public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_)
	{
		return new TileEntityRodSwitcher();
	}
}
