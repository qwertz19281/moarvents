package moarvents;

import ic2.core.block.reactor.tileentity.TileEntityNuclearReactorElectric;
import ic2.core.item.reactor.ItemReactorUranium;
import ic2.core.item.type.NuclearResourceType;
import ic2.core.ref.ItemName;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;

public class TileEntityRodSwitcher extends TileEntity implements ITickable {
	BlockPos down=null,up=null,east=null,south=null,west=null,north=null;
	TileEntity[] d=new TileEntity[6];// d,u,e,s,w,n;
	TileEntityNuclearReactorElectric[] rd=new TileEntityNuclearReactorElectric[6];
	int nu=0;
	@Override
	public void update() {
		nu++;
		if(nu<5) {
			return;
		}
		nu=0;
		if(world==null||world.isRemote){return;}
		if(down==null){
			down=new BlockPos(pos.getX(),pos.getY()-1,pos.getZ());
			up=new BlockPos(pos.getX(),pos.getY()+1,pos.getZ());
			east=new BlockPos(pos.getX()+1,pos.getY(),pos.getZ());
			south=new BlockPos(pos.getX(),pos.getY(),pos.getZ()+1);
			west=new BlockPos(pos.getX()-1,pos.getY(),pos.getZ());
			north=new BlockPos(pos.getX(),pos.getY(),pos.getZ()-1);
		}
		rd[0]=tryCast(MoarVents.searchForInterface(world,down,EnumFacing.DOWN));
		rd[1]=tryCast(MoarVents.searchForInterface(world,up,EnumFacing.UP));
		rd[2]=tryCast(MoarVents.searchForInterface(world,east,EnumFacing.EAST));
		rd[3]=tryCast(MoarVents.searchForInterface(world,south,EnumFacing.SOUTH));
		rd[4]=tryCast(MoarVents.searchForInterface(world,west,EnumFacing.WEST));
		rd[5]=tryCast(MoarVents.searchForInterface(world,north,EnumFacing.NORTH));
		for(int i=2;i<6;i++) {
			if(rd[i]!=null&&rd[1]!=null) {
				for(int x=0;x<rd[i].getReactorSize();x++) {
					for(int y=0;y<6;y++) {
						ItemStack stir=rd[i].getItemAt(x, y);
						if(isEmptyRod(stir)&&stir.getCount()>0) {
							boolean nodo=false;
							for(int j=0;j<rd[1].getReactorSize();j++) {
								for(int k=0;k<6;k++) {
									ItemStack stfu=rd[1].getItemAt(j, k);
									if(isFullRod(stfu)&&stfu.getCount()>0) {
										rd[1].setItemAt(j, k, stir);
										rd[i].setItemAt(x, y, stfu);
										nodo=true;
										break;
									}
								}
								if(nodo) {break;}
							}
						}
					}
				}
			}
			if(rd[i]!=null&&rd[0]!=null) {
				for(int x=0;x<rd[i].getReactorSize();x++) {
					for(int y=0;y<6;y++) {
						ItemStack stir=rd[i].getItemAt(x, y);
						if(isEmptyRod(stir)&&stir.getCount()>0) {
							boolean nodo=false;
							for(int j=0;j<rd[0].getReactorSize();j++) {
								for(int k=0;k<6;k++) {
									ItemStack stfu=rd[0].getItemAt(j, k);
									if(isFullRod(stfu)&&stfu.getCount()>0) {
										rd[0].setItemAt(j, k, stir);
										rd[i].setItemAt(x, y, stfu);
										nodo=true;
										break;
									}
								}
								if(nodo) {break;}
							}
						}
					}
				}
			}
		}//*/
		/*d[0]=world.getTileEntity(down);
		d[1]=world.getTileEntity(up);
		d[2]=world.getTileEntity(east);
		d[3]=world.getTileEntity(south);
		d[4]=world.getTileEntity(west);
		d[5]=world.getTileEntity(north);
		for(int i=0;i<6;i++) {
			rd[i]=searchForInterface(d[i]);
			if(rd[i]!=null) {
				swittch(rd[i]);
			}
		}//*/
	}
	/*public void swittch(TileEntityNuclearReactorElectric trg) {
		for(int x=0;x<trg.getReactorSize();x++) {
			for(int y=0;y<6;y++) {
				ItemStack stir=trg.getItemAt(x, y);
				if(isEmptyRod(stir)&&stir.getCount()>0) {
					int toRem=stir.getCount();
					ItemStack stirco=stir.copy();
					for(int i=0;i<6;i++) {
						if(d[i] instanceof IInventory && searchForInterface(d[i])==null&&toRem>0) {
							IInventory ci=(IInventory)d[i];
							for(int k=0;k<ci.getSizeInventory();k++) {
								if(ci.getStackInSlot(k).isItemEqual(stir)&&toRem>0) {
									ItemStack stcho=ci.getStackInSlot(k);
									int can=Math.min(ci.getInventoryStackLimit(),stcho.getMaxStackSize())-stcho.getCount();
									stcho.setCount(stcho.getCount()+Math.min(can, toRem));
									stir.setCount(stir.getCount()-Math.min(can, toRem));
								}
							}
							for(int k=0;k<ci.getSizeInventory();k++) {
								if(ci.getStackInSlot(k)==null||ci.getStackInSlot(k).isEmpty()&&toRem>0) {
									ItemStack stcho=stir.copy();
									int can=Math.min(ci.getInventoryStackLimit(),stcho.getMaxStackSize());
									stcho.setCount(Math.min(can, toRem));
									stir.setCount(stir.getCount()-Math.min(can, toRem));
								}
							}

						}
						if(toRem==0) {
							if(d[i] instanceof IInventory && searchForInterface(d[i])==null&&toRem>0) {
								IInventory ci=(IInventory)d[i];
								for(int j=0;j<ci.getSizeInventory();j++) {
									ItemStack stch=ci.getStackInSlot(j);
									if(isFullRod(stch)&&stch.getCount()>0) {
										if(!stir.isEmpty()) {System.err.println("FFFF");}
										ItemStack stchcp=stch.copy();
										stchcp.setCount(1);
										stch.setCount(stch.getCount()-1);
										trg.setItemAt(x, y, stchcp);
										toRem=-1;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	public static TileEntityNuclearReactorElectric searchForInterface(TileEntity te){
		if(te instanceof TileEntityNuclearReactorElectric){return (TileEntityNuclearReactorElectric)te;}
		else if(te instanceof IReactorChamber)
		{return (TileEntityNuclearReactorElectric)((IReactorChamber)te).getReactorInstance();}
		else if(te instanceof TileEntityReactorAccessHatch)
		{return (TileEntityNuclearReactorElectric)((TileEntityReactorAccessHatch)te).getReactorInstance();}// (sic!)
		return null;
	}*/
	public boolean isFullRod(ItemStack is) {
		if(is==null||is.isEmpty()) {return false;}
		return (is.getItem() instanceof ItemReactorUranium) && (!isEmptyRod(is));
	}
	ItemStack[] isarr=null;
	public boolean isEmptyRod(ItemStack is) {
		if(is==null||is.isEmpty()) {return false;}
		if(isarr==null) {
			isarr=new ItemStack[] {
					ItemName.nuclear.getItemStack(NuclearResourceType.depleted_mox),
					ItemName.nuclear.getItemStack(NuclearResourceType.depleted_uranium),
					ItemName.nuclear.getItemStack(NuclearResourceType.depleted_dual_mox),
					ItemName.nuclear.getItemStack(NuclearResourceType.depleted_dual_uranium),
					ItemName.nuclear.getItemStack(NuclearResourceType.depleted_quad_mox),
					ItemName.nuclear.getItemStack(NuclearResourceType.depleted_quad_uranium)
			};
		}
		for(int i=0;i<6;i++) {
			if(is.isItemEqual(isarr[i])) {
				return true;
			}
		}
		return false;
	}
	public TileEntityNuclearReactorElectric tryCast(Object obj) {
		if(obj instanceof TileEntityNuclearReactorElectric) {return (TileEntityNuclearReactorElectric)obj;}else{return null;}
	}
}
