package moarvents;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

import java.util.Calendar;

import ic2.api.reactor.IReactor;
import ic2.api.reactor.IReactorChamber;
import ic2.core.block.reactor.tileentity.TileEntityReactorAccessHatch;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;

@Mod(
		modid = "moarvents",
		name = "MoarVents",
		version = "beta1.2.5.2",
		updateJSON = "https://bitbucket.org/qwertz19281/moarvents/raw/master/update_info.json"
		)
public class MoarVents
{
	public static CRETAB ctab=null;

	static class CRETAB extends CreativeTabs {
		public CRETAB(String lable) {
			super(lable);
		}
		TexItem tabitem;
		ItemStack tis;

		@Override
		public ItemStack getTabIconItem()
		{
			return tis;
		}
	}
	//public static int lcnt = 9;
	//public static boolean selfSave = false;
	//public static boolean createEmitHeat = true;
	//public static boolean advLog = true;
	//public static Random rnd = new Random();
	public static BlockShaft shaft=null;
	public static BlockHeatConducter cndctr=null;
	public static BlockRHB rhb=null;
	public static BlockThermo thermo=null;
	public static BlockRdsw rdsw=null;
	public static BlockHEQ heq=null;
	public static TexBlock bconn=null;
	//public static BlockMaxKabel maxkabel=null;
	public static Item5x5Vent va=null;
	public static ItemFreezer vaB=null;
	public static TexItem[] mems=null;
	public static ItemOWHR owhr=null;public static ItemOWHD owhd=null;public static ItemOWHL owhl=null;public static ItemOWHU owhu=null;
	public static ItemColdBuffer coldt=null;
	public static ItemHotBuffer hott=null;
	public static ItemFireExtinguisher fireex=null;

	public static boolean foolDay=false;

	@EventHandler
	public void load(FMLInitializationEvent event){
		ctab.tabitem.setTextureName();
		va.setTextureName();
		vaB.setTextureName();
		for(int i=0;i<4;i++) {
			mems[i].setTextureName();
		}
		owhr.setTextureName();
		owhd.setTextureName();
		owhl.setTextureName();
		owhu.setTextureName();
		coldt.setTextureName();
		hott.setTextureName();
		fireex.setTextureName();

		cndctr.registerBlockTexture();
		rhb.registerBlockTexture();
		shaft.registerBlockTexture();
		thermo.registerBlockTexture();
		rdsw.registerBlockTexture();
		heq.registerBlockTexture();
		bconn.registerBlockTexture();
	}

	public static SoundEvent sre=new SoundEvent(new ResourceLocation("moarvents","fireex"));

	@EventHandler
	public void pre_load(FMLPreInitializationEvent event)
	{	
		ctab=new CRETAB("moarvents");
		try{
			Calendar calendar = Calendar.getInstance();
			if (calendar.get(Calendar.MONTH) == 3 && calendar.get(Calendar.DAY_OF_MONTH) == 1) {
				foolDay=true;
			}
			calendar=null;
			if(foolDay){
				try{
					throw new ClassNotFoundException("qwertz19281.moarvents.AprilFool");
				}catch(ClassNotFoundException ex){
					ex.printStackTrace();
				}
			}
		}catch(Exception ex){}

		System.out.println("MoarVents: Init Objects");

		/*for (ItemName name : ItemName.values) {
			System.out.println(name.name());
		}*/
		ctab.tabitem=new TexItem().setUnlocalizedNameB("mv_ctab_tex_item");
		shaft=new BlockShaft();
		cndctr=new BlockHeatConducter(Material.ROCK);
		rhb=new BlockRHB(Material.ROCK);
		thermo=new BlockThermo(Material.ROCK);
		rdsw=new BlockRdsw(Material.ROCK);
		heq=new BlockHEQ(Material.ROCK);
		bconn=(TexBlock) new TexBlock(Material.ROCK).setUnlocalizedName("bconn").setRegistryName("bconn").setCreativeTab(ctab);
		//maxkabel=new BlockMaxKabel(Material.cloth);
		va=new Item5x5Vent();
		vaB=new ItemFreezer();
		mems=new TexItem[]{(TexItem) 
				(new TexItem()).setUnlocalizedNameB("heatbuffer").setCreativeTab(CreativeTabs.MISC), 
				(TexItem) (new TexItem()).setUnlocalizedNameB("membrancomp").setCreativeTab(CreativeTabs.MISC), 
				(TexItem) (new TexItem()).setUnlocalizedNameB("membranliqheat").setCreativeTab(CreativeTabs.MISC), 
				(TexItem) (new TexItem()).setUnlocalizedNameB("membrantemp").setCreativeTab(CreativeTabs.MISC)};
		owhr=new ItemOWHR();
		owhd=new ItemOWHD();
		owhl=new ItemOWHL();
		owhu=new ItemOWHU();
		coldt=new ItemColdBuffer();
		hott=new ItemHotBuffer();
		fireex=new ItemFireExtinguisher();
		System.out.println("MoarVents: Register Objects");

		ForgeRegistries.ITEMS.register(ctab.tabitem);
		ctab.tis=new ItemStack(ctab.tabitem);

		int bc;

		for (bc=0;bc<4;bc++) {
			mems[bc].setCreativeTab(ctab);
		}

		ForgeRegistries.ITEMS.register(va);
		ForgeRegistries.ITEMS.register(vaB);
		ForgeRegistries.ITEMS.register(mems[0]);
		ForgeRegistries.ITEMS.register(mems[1]);
		ForgeRegistries.ITEMS.register(mems[2]);
		ForgeRegistries.ITEMS.register(mems[3]);
		ForgeRegistries.ITEMS.register(owhr);
		ForgeRegistries.ITEMS.register(owhd);
		ForgeRegistries.ITEMS.register(owhl);
		ForgeRegistries.ITEMS.register(owhu);
		ForgeRegistries.ITEMS.register(coldt);
		ForgeRegistries.ITEMS.register(hott);
		ForgeRegistries.ITEMS.register(fireex);
		TileEntity.register("shaft", TileEntityShaft.class);
		TileEntity.register("hc", TileEntityHeatConducter.class);
		TileEntity.register("rhb", TileEntityRHB.class);
		TileEntity.register("thermo", TileEntityThermo.class);
		TileEntity.register("rdsw", TileEntityRodSwitcher.class);
		TileEntity.register("heq", TileEntityHEQ.class);
		ForgeRegistries.BLOCKS.registerAll(cndctr,rhb,shaft,thermo,rdsw,heq,bconn);
		ForgeRegistries.ITEMS.registerAll(
				new ItemBlock(cndctr).setRegistryName("hc").setCreativeTab(ctab),
				new ItemBlock(rhb).setRegistryName("rhb").setCreativeTab(ctab),
				new ItemBlock(shaft).setRegistryName("shaft").setCreativeTab(ctab),
				new ItemBlock(thermo).setRegistryName("thermo").setCreativeTab(ctab),
				new ItemBlock(rdsw).setRegistryName("rdsw").setCreativeTab(ctab),
				new ItemBlock(heq).setRegistryName("heq").setCreativeTab(ctab),
				new ItemBlock(bconn).setRegistryName("bconn").setCreativeTab(ctab)
				);

		//ForgeRegistries.SOUND_EVENTS.register(sre.setRegistryName(sre.getSoundName()));
	}

	/*public void regBlock(Block b) {
		ForgeRegistries.BLOCKS.register(b);
		ForgeRegistries.ITEMS.register(new ItemBlock(b).setRegistryName(b.getRegistryName()).setCreativeTab(b.getCreativeTabToDisplayOn()));
	}*/

	public static IReactor searchForInterface(IBlockAccess w,BlockPos pos,EnumFacing todir){
		TileEntity te=w.getTileEntity(pos);
		if(te instanceof IReactor){return (IReactor)te;}
		else if(te instanceof IReactorChamber)
		{return (IReactor)((IReactorChamber)te).getReactorInstance();}
		else if(te instanceof TileEntityReactorAccessHatch)
		{return (IReactor)((TileEntityReactorAccessHatch)te).getReactorInstance();}// (sic!)
		if(w.getBlockState(pos).getBlock()==bconn) {return searchForInterface(w,pos.add(todir.getDirectionVec()),todir);}
		return null;
	}

	public static TileEntity getTE(IBlockAccess w,BlockPos pos,EnumFacing todir){
		if(w.getBlockState(pos).getBlock()==bconn) {return getTE(w,pos.add(todir.getDirectionVec()),todir);}
		return w.getTileEntity(pos);
	}
}
