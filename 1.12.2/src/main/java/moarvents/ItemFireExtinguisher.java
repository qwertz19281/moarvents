package moarvents;

import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.BlockFire;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class ItemFireExtinguisher extends TexItem {
	public ItemFireExtinguisher() {
		this.setCreativeTab(MoarVents.ctab);
		setMaxStackSize(1);
		this.setUnlocalizedNameB("fireex");
	}

	/*@Override
	public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		return EnumActionResult.SUCCESS;
	}*/

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn)
	{
		NBTTagCompound nbt=playerIn.getHeldItem(handIn).getTagCompound();
		if(nbt==null) {
			nbt=new NBTTagCompound();
			nbt.setInteger("extikk", 5);
			playerIn.getHeldItem(handIn).setTagCompound(nbt);
		}else {
			if(nbt.getInteger("exdura")>=1000) {return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, playerIn.getHeldItem(handIn));}
			nbt.setInteger("extikk", 5);
		}
		Vec3d pv=playerIn.getPositionVector();
		Vec3d lv=playerIn.getLookVec();
		worldIn.playSound(playerIn, pv.x+lv.x, pv.y+lv.y, pv.z+lv.z, MoarVents.sre, SoundCategory.NEUTRAL, 1.0f, 1.0f);
		return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, playerIn.getHeldItem(handIn));
	}



	@Override
	public int getMaxItemUseDuration(ItemStack stack)
	{
		return Integer.MAX_VALUE;
	}

	@Override
	public void onUpdate(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
		if(isSelected&&entityIn!=null) {
			NBTTagCompound nbt=stack.getTagCompound();
			int dam=0,dura=0;
			if(nbt!=null) {
				dam=nbt.getInteger("extikk")-1;
				dura=nbt.getInteger("exdura");
			}
			if(dura>=1000) {
				dura=1000;
				return;
			}
			if (dam>0) {
				//System.out.println("FESS");
				/*RayTraceResult rtr=rayTraceBlocksFire(worldIn,
						entityIn
						.getPositionVector(),
						entityIn
						.getPositionVector()
						.add(
								entityIn
								.getLookVec()
								.scale(12)), false,false,false);*/
				/*BlockPos bp=null;
				if() {
					if(worldIn.getBlockState(bp).getBlock() instanceof BlockFire) {
						worldIn.setBlockToAir(bp);
					}else{
						tryDelFire(worldIn,bp.down());
						tryDelFire(worldIn,bp.up());
						tryDelFire(worldIn,bp.east());
						tryDelFire(worldIn,bp.south());
						tryDelFire(worldIn,bp.west());
						tryDelFire(worldIn,bp.north());
					}
				}*/
				Vec3d v3d=entityIn.getPositionVector();
				v3d=v3d.addVector(0, entityIn.getEyeHeight(), 0);
				Vec3d v3d0=v3d.addVector(0,0,0);
				Vec3d lv=entityIn.getLookVec();
				lv=lv.scale(0.2);
				//System.out.println(lv.toString());
				BlockPos bp=new BlockPos(v3d);
				Block b=worldIn.getBlockState(bp).getBlock();
				if(b instanceof BlockFire) {
					worldIn.setBlockToAir(bp);
				}
				while(v3d0.distanceTo(v3d)<12) {
					v3d=v3d.add(lv);
					bp=new BlockPos(v3d);
					b=worldIn.getBlockState(bp).getBlock();
					if(b instanceof BlockFire) {
						worldIn.setBlockToAir(bp);
						dura++;
					}
					if(b instanceof BlockAir) {
						//worldIn.setBlockState(bp, Blocks.STONE.getDefaultState());
						/*dura+=tryDelFire(worldIn,bp.down());
						dura+=tryDelFire(worldIn,bp.up());
						dura+=tryDelFire(worldIn,bp.east());
						dura+=tryDelFire(worldIn,bp.south());
						dura+=tryDelFire(worldIn,bp.west());
						dura+=tryDelFire(worldIn,bp.north());*/
					}
					if(!(b instanceof BlockAir)) {
						/*dura+=tryDelFire(worldIn,bp.down());
						dura+=tryDelFire(worldIn,bp.up());
						dura+=tryDelFire(worldIn,bp.east());
						dura+=tryDelFire(worldIn,bp.south());
						dura+=tryDelFire(worldIn,bp.west());
						dura+=tryDelFire(worldIn,bp.north());*/
						break;
					}
				}

			}
			if(dam>=0) {
				if(nbt==null) {
					nbt=new NBTTagCompound();
					nbt.setInteger("extikk", dam);
					nbt.setInteger("exdura", dura);
					stack.setTagCompound(nbt);
				}else {
					nbt.setInteger("extikk", dam);
					nbt.setInteger("exdura", dura);
				}
			}
		}
	}

	public int tryDelFire(World w, BlockPos p) {
		if(w.getBlockState(p).getBlock() instanceof BlockFire) {
			w.setBlockToAir(p);
			return 1;
		}
		return 0;
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack)
    {
		if(stack.hasTagCompound()) {
			return ((double)(stack.getTagCompound().getInteger("exdura")))/1000d;
		}
		return 0;
    }
	
	@Override
	public boolean showDurabilityBar(ItemStack stack)
    {
        return true;
    }
}
