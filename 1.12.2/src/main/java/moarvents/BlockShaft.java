package moarvents;

import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockShaft extends TexBlock implements ITileEntityProvider
{
	public BlockShaft()
	{
		this(Material.ROCK);
	}

	public BlockShaft(Material mat)
	{
		super(mat);
		this.setUnlocalizedName("shaft");
		this.setRegistryName("shaft");
		//this.setBlockTextureName();
		this.setCreativeTab(MoarVents.ctab);
	}

	/**
	 * Returns a new instance of a block's tile entity class. Called on placing the block.
	 */
	@Override
	public TileEntity createNewTileEntity(World wld, int mat)
	{
		return new TileEntityShaft();
	}
}
