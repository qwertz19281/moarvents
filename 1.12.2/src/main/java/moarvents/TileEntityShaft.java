package moarvents;

import ic2.api.energy.tile.IKineticSource;
import ic2.core.block.reactor.tileentity.TileEntityNuclearReactorElectric;
import ic2.core.item.reactor.ItemReactorUranium;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;

public class TileEntityShaft extends TileEntity implements IKineticSource/*,ITickable*/
{
	BlockPos[] aa=null,ab=null,ac=null;
	@Override
	public int maxrequestkineticenergyTick(EnumFacing dirfrom)
	{
		if(aa==null) {
			aa=new BlockPos[] {
					new BlockPos(pos.getX(),pos.getY()+1,pos.getZ()),
					new BlockPos(pos.getX(),pos.getY()-1,pos.getZ()),
					new BlockPos(pos.getX(),pos.getY(),pos.getZ()+1),
					new BlockPos(pos.getX(),pos.getY(),pos.getZ()-1),
					new BlockPos(pos.getX()+1,pos.getY(),pos.getZ()),
					new BlockPos(pos.getX()-1,pos.getY(),pos.getZ())
			};
			ab=new BlockPos[] {
					new BlockPos(pos.getX(),pos.getY()+2,pos.getZ()),
					new BlockPos(pos.getX(),pos.getY()-2,pos.getZ()),
					new BlockPos(pos.getX(),pos.getY(),pos.getZ()+2),
					new BlockPos(pos.getX(),pos.getY(),pos.getZ()-2),
					new BlockPos(pos.getX()+2,pos.getY(),pos.getZ()),
					new BlockPos(pos.getX()-2,pos.getY(),pos.getZ())
			};
			ac=new BlockPos[] {
					new BlockPos(pos.getX(),pos.getY()+3,pos.getZ()),
					new BlockPos(pos.getX(),pos.getY()-3,pos.getZ()),
					new BlockPos(pos.getX(),pos.getY(),pos.getZ()+3),
					new BlockPos(pos.getX(),pos.getY(),pos.getZ()-3),
					new BlockPos(pos.getX()+3,pos.getY(),pos.getZ()),
					new BlockPos(pos.getX()-3,pos.getY(),pos.getZ())
			};
		}
		//EnumFacing fgd = dirfrom.getOpposite();
		//BlockPos bp=new BlockPos(pos.getX()+fgd.getFrontOffsetX(),pos.getY()+fgd.getFrontOffsetY(),pos.getZ()+fgd.getFrontOffsetZ());
		boolean shft=world.getBlockState(aa[dirfrom.getIndex()]).getBlock()==MoarVents.bconn;
		TileEntity te = this.world.getTileEntity((shft?ab:aa)[dirfrom.getIndex()]);
		if(te!=null&&te instanceof IKineticSource) {
			if(te instanceof TileEntityShaft) {
				return ((IKineticSource)te).getConnectionBandwidth(dirfrom);
			}else{
				TileEntity teb = this.world.getTileEntity((shft?ac:ab)[dirfrom.getIndex()]);
				if(teb!=null&&teb instanceof TileEntityShaft) {
					return (((IKineticSource)te).getConnectionBandwidth(dirfrom))+(((IKineticSource)teb).getConnectionBandwidth(dirfrom));
				}else {
					return ((IKineticSource)te).getConnectionBandwidth(dirfrom);
				}
			}
		}
		return 0;
		//bp=null;
		//return te != null && te instanceof IKineticSource ? ((IKineticSource)te).maxrequestkineticenergyTick(dirfrom) : 0;
	}
	@Override
	public int requestkineticenergy(EnumFacing dirfrom, int reqenergy)
	{
		if(aa==null) {
			aa=new BlockPos[] {
					new BlockPos(pos.getX(),pos.getY()+1,pos.getZ()),
					new BlockPos(pos.getX(),pos.getY()-1,pos.getZ()),
					new BlockPos(pos.getX(),pos.getY(),pos.getZ()+1),
					new BlockPos(pos.getX(),pos.getY(),pos.getZ()-1),
					new BlockPos(pos.getX()+1,pos.getY(),pos.getZ()),
					new BlockPos(pos.getX()-1,pos.getY(),pos.getZ())
			};
			ab=new BlockPos[] {
					new BlockPos(pos.getX(),pos.getY()+2,pos.getZ()),
					new BlockPos(pos.getX(),pos.getY()-2,pos.getZ()),
					new BlockPos(pos.getX(),pos.getY(),pos.getZ()+2),
					new BlockPos(pos.getX(),pos.getY(),pos.getZ()-2),
					new BlockPos(pos.getX()+2,pos.getY(),pos.getZ()),
					new BlockPos(pos.getX()-2,pos.getY(),pos.getZ())
			};
			ac=new BlockPos[] {
					new BlockPos(pos.getX(),pos.getY()+3,pos.getZ()),
					new BlockPos(pos.getX(),pos.getY()-3,pos.getZ()),
					new BlockPos(pos.getX(),pos.getY(),pos.getZ()+3),
					new BlockPos(pos.getX(),pos.getY(),pos.getZ()-3),
					new BlockPos(pos.getX()+3,pos.getY(),pos.getZ()),
					new BlockPos(pos.getX()-3,pos.getY(),pos.getZ())
			};
		}
		//EnumFacing fgd = dirfrom.getOpposite();
		//BlockPos bp=new BlockPos(pos.getX()+fgd.getFrontOffsetX(),pos.getY()+fgd.getFrontOffsetY(),pos.getZ()+fgd.getFrontOffsetZ());
		boolean shft=world.getBlockState(aa[dirfrom.getIndex()]).getBlock()==MoarVents.bconn;
		TileEntity te = this.world.getTileEntity((shft?ab:aa)[dirfrom.getIndex()]);
		if(te!=null&&te instanceof IKineticSource) {
			if(te instanceof TileEntityShaft) {
				return ((IKineticSource)te).drawKineticEnergy(dirfrom,reqenergy,false);
			}else{
				TileEntity teb = this.world.getTileEntity((shft?ac:ab)[dirfrom.getIndex()]);
				if(teb!=null&&teb instanceof TileEntityShaft) {
					return (((IKineticSource)te).drawKineticEnergy(dirfrom,reqenergy,false))+(((IKineticSource)teb).drawKineticEnergy(dirfrom,reqenergy,false));
				}else {
					return ((IKineticSource)te).drawKineticEnergy(dirfrom,reqenergy,false);
				}
			}
		}
		return 0;
		//return te != null && te instanceof IKineticSource ? ((IKineticSource)te).requestkineticenergy(dirfrom, reqenergy) : 0;
	}
	@Override
	public int drawKineticEnergy(EnumFacing dirfrom, int reqenergy,boolean sim)
	{
		if(aa==null) {
			aa=new BlockPos[] {
					new BlockPos(pos.getX(),pos.getY()+1,pos.getZ()),
					new BlockPos(pos.getX(),pos.getY()-1,pos.getZ()),
					new BlockPos(pos.getX(),pos.getY(),pos.getZ()+1),
					new BlockPos(pos.getX(),pos.getY(),pos.getZ()-1),
					new BlockPos(pos.getX()+1,pos.getY(),pos.getZ()),
					new BlockPos(pos.getX()-1,pos.getY(),pos.getZ())
			};
			ab=new BlockPos[] {
					new BlockPos(pos.getX(),pos.getY()+2,pos.getZ()),
					new BlockPos(pos.getX(),pos.getY()-2,pos.getZ()),
					new BlockPos(pos.getX(),pos.getY(),pos.getZ()+2),
					new BlockPos(pos.getX(),pos.getY(),pos.getZ()-2),
					new BlockPos(pos.getX()+2,pos.getY(),pos.getZ()),
					new BlockPos(pos.getX()-2,pos.getY(),pos.getZ())
			};
			ac=new BlockPos[] {
					new BlockPos(pos.getX(),pos.getY()+3,pos.getZ()),
					new BlockPos(pos.getX(),pos.getY()-3,pos.getZ()),
					new BlockPos(pos.getX(),pos.getY(),pos.getZ()+3),
					new BlockPos(pos.getX(),pos.getY(),pos.getZ()-3),
					new BlockPos(pos.getX()+3,pos.getY(),pos.getZ()),
					new BlockPos(pos.getX()-3,pos.getY(),pos.getZ())
			};
		}
		//EnumFacing fgd = dirfrom.getOpposite();
		//BlockPos bp=new BlockPos(pos.getX()+fgd.getFrontOffsetX(),pos.getY()+fgd.getFrontOffsetY(),pos.getZ()+fgd.getFrontOffsetZ());
		boolean shft=world.getBlockState(aa[dirfrom.getIndex()]).getBlock()==MoarVents.bconn;
		TileEntity te = this.world.getTileEntity((shft?ab:aa)[dirfrom.getIndex()]);
		if(te!=null&&te instanceof IKineticSource) {
			if(te instanceof TileEntityShaft) {
				return ((IKineticSource)te).drawKineticEnergy(dirfrom,reqenergy,sim);
			}else{
				TileEntity teb = this.world.getTileEntity((shft?ac:ab)[dirfrom.getIndex()]);
				if(teb!=null&&teb instanceof TileEntityShaft) {
					return (((IKineticSource)te).drawKineticEnergy(dirfrom,reqenergy,sim))+(((IKineticSource)teb).drawKineticEnergy(dirfrom,reqenergy,sim));
				}else {
					return ((IKineticSource)te).drawKineticEnergy(dirfrom,reqenergy,sim);
				}
			}
		}
		return 0;
		//return te != null && te instanceof IKineticSource ? ((IKineticSource)te).requestkineticenergy(dirfrom, reqenergy) : 0;
	}

	public void update() {
		TileEntityNuclearReactorElectric trg=(TileEntityNuclearReactorElectric)MoarVents.searchForInterface(world,new BlockPos(pos.getX(),pos.getY()-1,pos.getZ()),EnumFacing.DOWN);
		if(trg==null) {return;}
		for(int x=0;x<trg.getReactorSize();x++) {
			for(int y=0;y<6;y++) {
				ItemStack is=trg.getItemAt(x, y);
				if(is!=null && !is.isEmpty() && (is.getItem() instanceof ItemReactorUranium)) {
					ItemReactorUranium ris=(ItemReactorUranium)is.getItem();
					int tdam=ris.getMaxCustomDamage(is)-4;
					if(ris.getCustomDamage(is)<tdam) {
						ris.setCustomDamage(is, tdam);
					}
				}
			}
		}
	}
}
