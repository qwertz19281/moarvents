package moarvents;

import ic2.api.energy.tile.IHeatSource;
import ic2.api.reactor.IReactor;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;

public class TileEntityRHB extends TileEntity implements IHeatSource,ITickable
{
	int ticker=0,datmax=0,dattransed=0;double hi;
	boolean inSec;
	
	public void update() {
		if(world==null||world.isRemote){return;}
		ticker++;
		if(ticker==20){
			ticker=0;
			inSec=false;
			searchForHeatAndInjectIntoReactor(true);
		} else {
			searchForHeatAndInjectIntoReactor(false);
		}
	}
	
	public boolean canUpdate()
	{
		return true;
	}
	
	public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        ticker=tag.getInteger("tickah");
        datmax=tag.getInteger("datmax");
        dattransed=tag.getInteger("dattransed");
        inSec=tag.getBoolean("inSec");
        hi=tag.getDouble("hi");
    }

    public NBTTagCompound writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        tag.setInteger("tickah", ticker);
        tag.setInteger("datmax", datmax);
        tag.setInteger("dattransed", dattransed);
        tag.setBoolean("inSec", inSec);
        tag.setDouble("hi", hi);
        return tag;
    }

    public void searchForHeatAndInjectIntoReactor(boolean inject){
    	//int heattopull=256,heatpulled=0;
    	if(down==null){
			down=new BlockPos(pos.getX(),pos.getY()-1,pos.getZ());
	    	up=new BlockPos(pos.getX(),pos.getY()+1,pos.getZ());
	    	east=new BlockPos(pos.getX()+1,pos.getY(),pos.getZ());
	    	south=new BlockPos(pos.getX(),pos.getY(),pos.getZ()+1);
	    	west=new BlockPos(pos.getX()-1,pos.getY(),pos.getZ());
	    	north=new BlockPos(pos.getX(),pos.getY(),pos.getZ()-1);
		}
    	TileEntity
    		dt=MoarVents.getTE(world,down,EnumFacing.DOWN),
    		ut=MoarVents.getTE(world,up,EnumFacing.UP),
    		et=MoarVents.getTE(world,east,EnumFacing.EAST),
    		st=MoarVents.getTE(world,south,EnumFacing.SOUTH),
    		wt=MoarVents.getTE(world,west,EnumFacing.WEST),
    		nt=MoarVents.getTE(world,north,EnumFacing.NORTH);
    	IHeatSource sd=getHeatSourceNotSelf(dt);
		IHeatSource su=getHeatSourceNotSelf(ut);
		IHeatSource se=getHeatSourceNotSelf(et);
		IHeatSource ss=getHeatSourceNotSelf(st);
		IHeatSource sw=getHeatSourceNotSelf(wt);
		IHeatSource sn=getHeatSourceNotSelf(nt);
		IReactor d=MoarVents.searchForInterface(world,down,EnumFacing.DOWN);
		IReactor u=MoarVents.searchForInterface(world,up,EnumFacing.UP);
		IReactor e=MoarVents.searchForInterface(world,east,EnumFacing.EAST);
		IReactor s=MoarVents.searchForInterface(world,south,EnumFacing.SOUTH);
		IReactor w=MoarVents.searchForInterface(world,west,EnumFacing.WEST);
		IReactor n=MoarVents.searchForInterface(world,north,EnumFacing.NORTH);
		int count=0;
		if(d!=null){count++;}
		if(u!=null){count++;}
		if(e!=null){count++;}
		if(s!=null){count++;}
		if(w!=null){count++;}
		if(n!=null){count++;}
		//down=null;up=null;east=null;south=null;west=null;north=null;dt=null;ut=null;et=null;st=null;wt=null;nt=null;
		if(count==0){return;}
		//NOW PULL ALL HEAT WHICH IS PULLABLE
		if(sd!=null){hi+=sd.drawHeat(EnumFacing.UP, Math.min(sd.getConnectionBandwidth(EnumFacing.UP),(int)(4096-hi)),false);}
		if(su!=null){hi+=su.drawHeat(EnumFacing.DOWN, Math.min(su.getConnectionBandwidth(EnumFacing.DOWN),(int)(4096-hi)),false);}
		if(se!=null){hi+=se.drawHeat(EnumFacing.WEST, Math.min(se.getConnectionBandwidth(EnumFacing.WEST),(int)(4096-hi)),false);}
		if(ss!=null){hi+=ss.drawHeat(EnumFacing.NORTH, Math.min(ss.getConnectionBandwidth(EnumFacing.NORTH),(int)(4096-hi)),false);}
		if(sw!=null){hi+=sw.drawHeat(EnumFacing.EAST, Math.min(sw.getConnectionBandwidth(EnumFacing.EAST),(int)(4096-hi)),false);}
		if(sn!=null){hi+=sn.drawHeat(EnumFacing.SOUTH, Math.min(sn.getConnectionBandwidth(EnumFacing.SOUTH),(int)(4096-hi)),false);}
		if(!inject){return;}
		int totransfer=(int)(hi/count/40);
		hi-=totransfer*count*40;
		if(d!=null){d.addHeat(totransfer);}
		if(u!=null){u.addHeat(totransfer);}
		if(e!=null){e.addHeat(totransfer);}
		if(s!=null){s.addHeat(totransfer);}
		if(w!=null){w.addHeat(totransfer);}
		if(n!=null){n.addHeat(totransfer);}
    }
    
	@Override
	public int maxrequestHeatTick(EnumFacing directionFrom) {
		// TODO Auto-generated method stub
		if(down==null){
			down=new BlockPos(pos.getX(),pos.getY()-1,pos.getZ());
	    	up=new BlockPos(pos.getX(),pos.getY()+1,pos.getZ());
	    	east=new BlockPos(pos.getX()+1,pos.getY(),pos.getZ());
	    	south=new BlockPos(pos.getX(),pos.getY(),pos.getZ()+1);
	    	west=new BlockPos(pos.getX()-1,pos.getY(),pos.getZ());
	    	north=new BlockPos(pos.getX(),pos.getY(),pos.getZ()-1);
		}
		IReactor d=MoarVents.searchForInterface(world,down,EnumFacing.DOWN);
		IReactor u=MoarVents.searchForInterface(world,up,EnumFacing.UP);
		IReactor e=MoarVents.searchForInterface(world,east,EnumFacing.EAST);
		IReactor s=MoarVents.searchForInterface(world,south,EnumFacing.SOUTH);
		IReactor w=MoarVents.searchForInterface(world,west,EnumFacing.WEST);
		IReactor n=MoarVents.searchForInterface(world,north,EnumFacing.NORTH);
		/*int count=0;
		if(d!=null){count++;}
		if(u!=null){count++;}
		if(e!=null){count++;}
		if(s!=null){count++;}
		if(w!=null){count++;}
		if(n!=null){count++;}*/
		int max=0;
		if(d!=null){max+=d.getHeat();}
		if(u!=null){max+=u.getHeat();}
		if(e!=null){max+=e.getHeat();}
		if(s!=null){max+=s.getHeat();}
		if(w!=null){max+=w.getHeat();}
		if(n!=null){max+=n.getHeat();}
		//return max*2;
		//System.out.print("REQ"+max);
		//down=null;up=null;east=null;south=null;west=null;north=null;
		if(max==0){
			if(inSec){
				//System.out.println(" "+max*2+" T"+ticker+" IS"+inSec);
				return datmax;
			} else {
				//System.out.println(" 0 T"+ticker+" IS"+inSec);
				return 0;
			}
		} else {
			inSec=true;
			ticker=0;
			datmax=max*2;
			//System.out.println(" "+max*2+" T"+ticker+" IS"+inSec);
			return max*2;
		}
	}

	public BlockPos down=null,up=null,east=null,south=null,west=null,north=null;
	
	@Override
	public int requestHeat(EnumFacing directionFrom, int requestheat) {
		// TODO Auto-generated method stub
		if(requestheat<1){return 0;}
		IReactor d=MoarVents.searchForInterface(world,down,EnumFacing.DOWN);
		IReactor u=MoarVents.searchForInterface(world,up,EnumFacing.UP);
		IReactor e=MoarVents.searchForInterface(world,east,EnumFacing.EAST);
		IReactor s=MoarVents.searchForInterface(world,south,EnumFacing.SOUTH);
		IReactor w=MoarVents.searchForInterface(world,west,EnumFacing.WEST);
		IReactor n=MoarVents.searchForInterface(world,north,EnumFacing.NORTH);
		int count=0,available=0;
		if(d!=null){count++;available+=d.getHeat();}
		if(u!=null){count++;available+=u.getHeat();}
		if(e!=null){count++;available+=e.getHeat();}
		if(s!=null){count++;available+=s.getHeat();}
		if(w!=null){count++;available+=w.getHeat();}
		if(n!=null){count++;available+=n.getHeat();}
		int trans=Math.min(requestheat/2,available);
		int subtrans=trans/count; // ROUND ROBIN
		int transed=0;
		if(d!=null){
			int tomove=Math.min(subtrans,d.getHeat());
			d.addHeat(-tomove);
			transed+=tomove;
		}
		if(u!=null){
			int tomove=Math.min(subtrans,u.getHeat());
			u.addHeat(-tomove);
			transed+=tomove;
		}
		if(e!=null){
			int tomove=Math.min(subtrans,e.getHeat());
			e.addHeat(-tomove);
			transed+=tomove;
		}
		if(s!=null){
			int tomove=Math.min(subtrans,s.getHeat());
			s.addHeat(-tomove);
			transed+=tomove;
		}
		if(w!=null){
			int tomove=Math.min(subtrans,w.getHeat());
			w.addHeat(-tomove);
			transed+=tomove;
		}
		if(n!=null){
			int tomove=Math.min(subtrans,n.getHeat());
			n.addHeat(-tomove);
			transed+=tomove;
		}
		//System.out.print("DO"+transed);
		/*if(transed==0){
			if(inSec){
				//System.out.println(" "+transed*2+" T"+ticker+" IS"+inSec);
				return dattransed;
			} else {
				//System.out.println(" 0 T"+ticker+" IS"+inSec);
				return 0;
			}
		} else {
			inSec=true;
			ticker=0;
			dattransed=transed*2;
			//System.out.println(" "+transed*2+" T"+ticker+" IS"+inSec);
			return transed*2;
		}*/
		//down=null;up=null;east=null;south=null;west=null;north=null;
		return transed*40;
		//return transed*2;
	}
	
	public IHeatSource getHeatSourceNotSelf(TileEntity te){
		if(te instanceof TileEntityRHB){return null;}
		if(te instanceof IHeatSource){return (IHeatSource)te;}
		return null;
	}
}
