package qwertz19281.moarvents;

import ic2.api.energy.tile.IKineticSource;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.ForgeDirection;

public class TileEntityShaft extends TileEntity implements IKineticSource
{
	public boolean canUpdate()
	{
		return false;
	}

	public int maxrequestkineticenergyTick(ForgeDirection dirfrom)
	{
		ForgeDirection opp = dirfrom.getOpposite();
		TileEntity te = this.worldObj.getTileEntity(xCoord + opp.offsetX, yCoord + opp.offsetY, zCoord + opp.offsetZ);
		return te != null && te instanceof IKineticSource ? ((IKineticSource)te).maxrequestkineticenergyTick(dirfrom) : 0;
	}

	public int requestkineticenergy(ForgeDirection dirfrom, int reqenergy)
	{
		ForgeDirection opp = dirfrom.getOpposite();
		TileEntity te = this.worldObj.getTileEntity(xCoord + opp.offsetX, yCoord + opp.offsetY, zCoord + opp.offsetZ);
		return te != null && te instanceof IKineticSource ? ((IKineticSource)te).requestkineticenergy(dirfrom, reqenergy) : 0;
	}
}
