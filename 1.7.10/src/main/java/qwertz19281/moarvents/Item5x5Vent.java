package qwertz19281.moarvents;

import java.util.HashMap;
import java.util.Map;

import ic2.api.reactor.IReactor;
import ic2.api.reactor.IReactorComponent;
import ic2.core.block.reactor.tileentity.TileEntityNuclearReactorElectric;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

public class Item5x5Vent extends Item implements IReactorComponent {

	public Item5x5Vent()
	{
		this.setTextureName("moarvents:abzuier5");
		this.setMaxStackSize(64);
		this.setCreativeTab(MoarVents.ctab);
		this.setUnlocalizedName("ventAbzuier");
	}
	
	@Override
	public void processChamber(IReactor reactor, ItemStack yourStack, int x, int y, boolean heatrun) {
		if(!heatrun) {
			toemit.remove(reactor);
		}
	}

	@Override
	public boolean acceptUraniumPulse(IReactor reactor, ItemStack yourStack, ItemStack pulsingStack, int youX, int youY,
			int pulseX, int pulseY, boolean heatrun) {
		return false;
	}

	@Override
	public boolean canStoreHeat(IReactor reactor, ItemStack yourStack, int x, int y) {
		return true;
	}

	@Override
	public int getMaxHeat(IReactor reactor, ItemStack yourStack, int x, int y) {
		return Integer.MAX_VALUE;
	}

	@Override
	public int getCurrentHeat(IReactor reactor, ItemStack yourStack, int x, int y) {
		return 0;
	}

	@Override
	public int alterHeat(IReactor reactor, ItemStack yourStack, int x, int y, int heat) {
		int haet=heat;
		if(haet<1){return haet;}
		//haet=Math.min(reactor.getMaxHeat()-1-reactor.getHeat(), haet);
				if(reactor.isFluidCooled()){
					int ae=0;
					Integer iiii=toemit.get(reactor);
					if(iiii!=null) {
						ae=iiii.intValue();
					}
					TileEntityNuclearReactorElectric nte=(TileEntityNuclearReactorElectric) reactor;
					haet=Math.min((Math.min(nte.inputTank.getFluidAmount(),nte.outputTank.getCapacity()-nte.outputTank.getFluidAmount())-ae)/2,haet);
					if(haet>0) {
						reactor.addEmitHeat(haet);
						toemit.put(reactor, new Integer(ae+haet*2));
						return heat-haet;
					} else {
						return heat;
					}//*/
					
				}else{return reactor.addHeat(Math.min((int)(0.8*reactor.getMaxHeat())-1-reactor.getHeat(),haet))-haet;}
				//return 0;
	}

	@Override
	public float influenceExplosion(IReactor reactor, ItemStack yourStack) {
		return 0;
	}

	public Map<IReactor,Integer> toemit=new HashMap<IReactor,Integer>();
}
