package qwertz19281.moarvents;

import org.apache.commons.lang3.mutable.MutableObject;

import ic2.core.IC2;
import ic2.core.block.wiring.TileEntityCable;
import ic2.core.util.LogCategory;
import ic2.core.util.Util;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.util.ForgeDirection;

public class BlockMaxKabel extends Block implements ITileEntityProvider {

	protected BlockMaxKabel(Material mat) {
		super(mat);
		this.setBlockName("mv_maxkabel");
		this.setBlockTextureName("moarvents:maxkabel");
		this.setCreativeTab(MoarVents.ctab);
	}

	@Override
	public TileEntity createNewTileEntity(World wld, int meta) {
		// TODO Auto-generated method stub
		return new TEMaxKabel();
	}

	@Override
	public int getFlammability(IBlockAccess world, int x, int y, int z, ForgeDirection face)
	{
		return 0;
	}
	
	public void onNeighborBlockChange(World world, int x, int y, int z, Block neighbor)
	  {
	    super.onNeighborBlockChange(world, x, y, z, neighbor);
	    if (IC2.platform.isSimulating())
	    {
	      TileEntityCable te = (TileEntityCable)getOwnTe(world, x, y, z);
	      if (te == null) {
	        return;
	      }
	      te.onNeighborBlockChange();
	    }
	  }
	
	public TileEntity getOwnTe(IBlockAccess blockAccess, int x, int y, int z)
	  {
	    TileEntity te;
	    Block block;
	    int meta;
	    if ((blockAccess instanceof World))
	    {
	      Chunk chunk = Util.getLoadedChunk((World)blockAccess, x >> 4, z >> 4);
	      if (chunk == null) {
	        return null;
	      }
	      block = chunk.getBlock(x & 0xF, y, z & 0xF);
	      meta = chunk.getBlockMetadata(x & 0xF, y, z & 0xF);
	      
	      te = blockAccess.getTileEntity(x, y, z);
	    }
	    else
	    {
	      block = blockAccess.getBlock(x, y, z);
	      meta = blockAccess.getBlockMetadata(x, y, z);
	      te = blockAccess.getTileEntity(x, y, z);
	    }
	    Class<? extends TileEntity> expectedClass = getTeClass(meta, null, null);
	    Class<? extends TileEntity> actualClass = te != null ? te.getClass() : null;
	    if (actualClass != expectedClass)
	    {
	      if (block != this)
	      {
	        if (Util.inDev())
	        {
	          StackTraceElement[] st = new Throwable().getStackTrace();
	          IC2.log.warn(LogCategory.Block, "Own tile entity query from %s to foreign block %s instead of %s at %s.", new Object[] { st.length > 1 ? st[1] : "?", block != null ? block
	          
	            .getClass() : null, 
	            getClass(), 
	            Util.formatPosition(blockAccess, x, y, z) });
	        }
	        return null;
	      }
	      IC2.log.warn(LogCategory.Block, "Mismatched tile entity at %s, got %s, expected %s.", new Object[] {
	        Util.formatPosition(blockAccess, x, y, z), actualClass, expectedClass });
	      if ((blockAccess instanceof World))
	      {
	        World world = (World)blockAccess;
	        
	        te = createTileEntity(world, meta);
	        world.setTileEntity(x, y, z, te);
	      }
	      else
	      {
	        return null;
	      }
	    }
	    return te;
	  }
	
	public Class<? extends TileEntity> getTeClass(int meta, MutableObject<Class<?>[]> ctorArgTypes, MutableObject<Object[]> ctorArgs){
		return TEMaxKabel.class;
	}
}
