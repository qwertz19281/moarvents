package qwertz19281.moarvents;

import ic2.api.reactor.IReactor;
import ic2.api.reactor.IReactorComponent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class ItemHotTasche extends Item implements IReactorComponent {

	public ItemHotTasche()
	{
		this.setTextureName("moarvents:packHot");
		this.setMaxStackSize(1);
		this.setCreativeTab(MoarVents.ctab);
		this.setUnlocalizedName("heatHot");
	}
	
	@Override
	public void processChamber(IReactor reactor, ItemStack baum, int x, int y, boolean heatrun) {
		// TODO Auto-generated method stub
		if(heatrun){
			NBTTagCompound data=baum.getTagCompound();
			if(data==null){data=new NBTTagCompound();baum.setTagCompound(data);}
			int tier=data.getInteger("taschetier");
			if(tier==0){logfunc("ERROR: Tier of "+baum.toString()+"=0");tier=INIT_TIER;
				data.setInteger("taschetier", INIT_TIER);
			}
		
			int medh=33*tier;
			int heat=66*tier+1;
			if(data.hasKey("tascheheat")) {
				heat=data.getInteger("tascheheat");
			} else {
				data.setInteger("tascheheat", heat);
				baum.setItemDamage(heat/tier);
			}
			
			//int div=0;
			
			ItemStack right=reactor.getItemAt(x+1, y);//if(right!=null){div++;}
			ItemStack bottom=reactor.getItemAt(x, y+1);//if(bottom!=null){div++;}
			ItemStack left=reactor.getItemAt(x-1, y);//if(left!=null){div++;}
			ItemStack top=reactor.getItemAt(x, y-1);//if(top!=null){div++;}
			
			//if(div==0){return;}
			
			//int hdd=(heat/div);
			
			try{if(heat>0&&right!=null&&((IReactorComponent)right.getItem()).canStoreHeat(reactor, right, x+1, y)&&((((IReactorComponent)right.getItem()).getCurrentHeat(reactor, right, x+1, y)*2)<((IReactorComponent)right.getItem()).getMaxHeat(reactor, right, x+1, y))){
				int doheat=Math.min(heat, ((IReactorComponent)right.getItem()).getMaxHeat(reactor, right, x+1, y)/2)-((IReactorComponent)right.getItem()).getCurrentHeat(reactor, right, x+1, y);
				if(doheat>0){
					heat-=(doheat-((IReactorComponent)right.getItem()).alterHeat(reactor, right, x+1, y, doheat));
				}
			}}catch(ClassCastException ex) {}
			try{if(heat>0&&bottom!=null&&((IReactorComponent)bottom.getItem()).canStoreHeat(reactor, bottom, x, y+1)&&((((IReactorComponent)bottom.getItem()).getCurrentHeat(reactor, bottom, x, y+1)*2)<((IReactorComponent)bottom.getItem()).getMaxHeat(reactor, bottom, x, y+1))){
				int doheat=Math.min(heat, ((IReactorComponent)bottom.getItem()).getMaxHeat(reactor, bottom, x, y+1)/2)-((IReactorComponent)bottom.getItem()).getCurrentHeat(reactor, bottom, x, y+1);
				if(doheat>0){
					heat-=(doheat-((IReactorComponent)bottom.getItem()).alterHeat(reactor, bottom, x, y+1, doheat));
				}
			}}catch(ClassCastException ex) {}
			try{if(heat>0&&left!=null&&((IReactorComponent)left.getItem()).canStoreHeat(reactor, left, x-1, y)&&((((IReactorComponent)left.getItem()).getCurrentHeat(reactor, left, x-1, y)*2)<((IReactorComponent)left.getItem()).getMaxHeat(reactor, left, x-1, y))){
				int doheat=Math.min(heat, ((IReactorComponent)left.getItem()).getMaxHeat(reactor, left, x-1, y)/2)-((IReactorComponent)left.getItem()).getCurrentHeat(reactor, left, x-1, y);
				if(doheat>0){
					heat-=(doheat-((IReactorComponent)left.getItem()).alterHeat(reactor, left, x-1, y, doheat));
				}
			}}catch(ClassCastException ex) {}
			try{if(heat>0&&top!=null&&((IReactorComponent)top.getItem()).canStoreHeat(reactor, top, x, y-1)&&((((IReactorComponent)top.getItem()).getCurrentHeat(reactor, top, x, y-1)*2)<((IReactorComponent)top.getItem()).getMaxHeat(reactor, top, x, y-1))){
				int doheat=Math.min(heat, ((IReactorComponent)top.getItem()).getMaxHeat(reactor, top, x, y-1)/2)-((IReactorComponent)top.getItem()).getCurrentHeat(reactor, top, x, y-1);
				if(doheat>0){
					heat-=(doheat-((IReactorComponent)top.getItem()).alterHeat(reactor, top, x, y-1, doheat));
				}
			}}catch(ClassCastException ex) {}
			if(heat<=medh){
				makeItCold(reactor,baum,x,y,tier,heat);
				return;
			}
			data.setInteger("tascheheat", heat);
			baum.setItemDamage(heat/tier);
		}
		/*if(reactor.getHeat()>=reactor.getMaxHeat()) {
			NBTTagCompound data=baum.getTagCompound();
			if(data==null){data=new NBTTagCompound();baum.setTagCompound(data);}
			int tier=data.getInteger("taschetier");
			if(tier==0){logfunc("ERROR: Tier of "+baum.toString()+"=0");tier=INIT_TIER;
				data.setInteger("taschetier", INIT_TIER);
			}
		
			int medh=33*tier;
			int heat=66*tier+1;
			if(data.hasKey("tascheheat")) {
				heat=data.getInteger("tascheheat");
			} else {
				data.setInteger("tascheheat", heat);
				baum.setItemDamage(heat/tier);
			}
			
			int hm=Math.min(reactor.getHeat()-(reactor.getMaxHeat()-1),reactor.getMaxHeat())
		}*/
	}

	@Override
	public boolean acceptUraniumPulse(IReactor reactor, ItemStack baum, ItemStack pulsingStack, int youX, int youY,
			int pulseX, int pulseY, boolean heatrun) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canStoreHeat(IReactor reactor, ItemStack baum, int x, int y) {
		// TODO Auto-generated method stub
		return true;
	}

	public static final int INIT_TIER=28;
	
	@Override
	public int getMaxHeat(IReactor reactor, ItemStack baum, int x, int y) {
		// TODO Auto-generated method stub
		NBTTagCompound data=baum.getTagCompound();
		if(data==null){data=new NBTTagCompound();baum.setTagCompound(data);}
		int tier=data.getInteger("taschetier");
		if(tier==0){logfunc("ERROR: Tier of "+baum.toString()+"=0");tier=INIT_TIER;
			data.setInteger("taschetier", INIT_TIER);
		}
		int maxh=100*tier;
		return maxh;
	}

	@Override
	public int getCurrentHeat(IReactor reactor, ItemStack baum, int x, int y) {
		NBTTagCompound data=baum.getTagCompound();
		if(data==null){data=new NBTTagCompound();baum.setTagCompound(data);}
		int tier=data.getInteger("taschetier");
		if(tier==0){logfunc("ERROR: Tier of "+baum.toString()+"=0");tier=INIT_TIER;
			data.setInteger("taschetier", INIT_TIER);
		}
	
		int medh=33*tier;
		int heat=66*tier+1;
		if(data.hasKey("tascheheat")) {
			heat=data.getInteger("tascheheat");
		} else {
			data.setInteger("tascheheat", heat);
			baum.setItemDamage(heat/tier);
		}
		return heat;
	}

	@Override
	public int alterHeat(IReactor reactor, ItemStack baum, int x, int y, int hiid) {
		NBTTagCompound data=baum.getTagCompound();
		if(data==null){data=new NBTTagCompound();baum.setTagCompound(data);}
		int tier=data.getInteger("taschetier");
		if(tier==0){logfunc("ERROR: Tier of "+baum.toString()+"=0");tier=INIT_TIER;
			data.setInteger("taschetier", INIT_TIER);
		}
	
		int medh=33*tier;
		int heat=66*tier+1;
		if(data.hasKey("tascheheat")) {
			heat=data.getInteger("tascheheat");
		} else {
			data.setInteger("tascheheat", heat);
			baum.setItemDamage(heat/tier);
		}
		logfunc("Prev heat "+heat);
		logfunc("Ins heat "+hiid);
		int maxh=100*tier;
		int simh=heat;
		int didh=hiid;
		simh+=didh;
		while(simh<0){
			didh-=simh;
			simh=heat;
			simh+=didh;
		}
		while(simh>maxh){
			didh-=(simh-maxh);
			simh=heat;
			simh+=didh;
		}
		if(simh!=heat){
			heat=simh;
			if(heat<=medh){
				makeItCold(reactor,baum,x,y,tier,heat);
				return didh;
			}
			data.setInteger("tascheheat", heat);
			logfunc("Store heat "+heat);
			baum.setItemDamage(heat/tier);
		} else {
			if(heat<=medh){
				makeItCold(reactor,baum,x,y,tier,heat);
			}
		}
		logfunc("ret heat "+didh);
		return hiid-didh;
	}

	@Override
	public float influenceExplosion(IReactor reactor, ItemStack baum) {
		NBTTagCompound data=baum.getTagCompound();
		if(data==null){data=new NBTTagCompound();baum.setTagCompound(data);}
		int tier=data.getInteger("taschetier");
		if(tier==0){logfunc("ERROR: Tier of "+baum.toString()+"=0");tier=INIT_TIER;
			data.setInteger("taschetier", INIT_TIER);
		}
	
		int medh=33*tier;
		int heat=66*tier+1;
		if(data.hasKey("tascheheat")) {
			heat=data.getInteger("tascheheat");
		} else {
			data.setInteger("tascheheat", heat);
			baum.setItemDamage(heat/tier);
		}
		return 0.8f+(0.002f*((float)heat/(float)tier));
	}
	
	public void makeItCold(IReactor inv, ItemStack stack, int x, int y, int tier, int heat) {
		int maxh=100*tier;
		ItemStack newStack = new ItemStack(MoarVents.coldt, 1, maxh);
		NBTTagCompound nbttree=newStack.getTagCompound();
		if(nbttree==null){nbttree=new NBTTagCompound();newStack.setTagCompound(nbttree);}
		nbttree.setInteger("tascheheat", heat);
		nbttree.setInteger("taschetier", tier);
		newStack.setItemDamage(heat/tier);
		inv.setItemAt(x, y, newStack);
	}
	
	@Override
	public int getMaxDamage(ItemStack stack)
    {
		return 100;
	}
	
	public static void logfunc(Object... dta){}
}
