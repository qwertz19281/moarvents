package moarvents;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.util.Calendar;
import java.util.Random;
import java.util.logging.Logger;

import ic2.api.item.IC2Items;
import ic2.api.reactor.IReactor;
import ic2.api.reactor.IReactorChamber;
import ic2.core.block.reactor.tileentity.TileEntityReactorAccessHatch;
import ic2.core.item.type.CraftingItemType;
import ic2.core.ref.ItemName;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.oredict.ShapedOreRecipe;

@Mod(
		modid = "moarvents",
		name = "MoarVents",
		version = "beta1.2.5.2",
		updateJSON = "https://bitbucket.org/qwertz19281/moarvents/raw/master/update_info.json"
		)
public class MoarVents
{
	public static CRETAB ctab=null;

	static class CRETAB extends CreativeTabs {
		public CRETAB(String lable) {
			super(lable);
		}
		Item tabitem;
		public Item getTabIconItem()
		{
			return tabitem;
		}
	}
	//public static int lcnt = 9;
	//public static boolean selfSave = false;
	//public static boolean createEmitHeat = true;
	//public static boolean advLog = true;
	//public static Random rnd = new Random();
	public static BlockShaft shaft=null;
	public static BlockHeatConducter cndctr=null;
	public static BlockRHB rhb=null;
	public static BlockThermo thermo=null;
	public static BlockRdsw rdsw=null;
	public static BlockHEQ heq=null;
	public static TexBlock bconn=null;
	//public static BlockMaxKabel maxkabel=null;
	public static Item5x5Vent va=null;
	public static ItemFreezer vaB=null;
	public static TexItem[] mems=null;
	public static ItemOWHR owhr=null;public static ItemOWHD owhd=null;public static ItemOWHL owhl=null;public static ItemOWHU owhu=null;
	public static ItemColdBuffer coldt=null;
	public static ItemHotBuffer hott=null;
	public static ItemFireExtinguisher fireex=null;

	public static boolean foolDay=false;

	@EventHandler
	public void preload(FMLPreInitializationEvent event){
		ctab=new CRETAB("moarvents");
	}
	
	@EventHandler
	public void load(FMLInitializationEvent event)
	{	
		try{
			Calendar calendar = Calendar.getInstance();
			if (calendar.get(calendar.MONTH) == 3 && calendar.get(calendar.DAY_OF_MONTH) == 1) {
				foolDay=true;
			}
			calendar=null;
			if(foolDay){
				try{
					throw new ClassNotFoundException("qwertz19281.moarvents.AprilFool");
				}catch(ClassNotFoundException ex){
					ex.printStackTrace();
				}
			}
		}catch(Exception ex){}

		System.out.println("MoarVents: Init Objects");

		/*for (ItemName name : ItemName.values) {
			System.out.println(name.name());
		}*/
		ctab.tabitem=new TexItem().setUnlocalizedNameB("mv_ctab_tex_item").setTextureName();
		shaft=new BlockShaft();
		cndctr=new BlockHeatConducter(Material.ROCK);
		rhb=new BlockRHB(Material.ROCK);
		thermo=new BlockThermo(Material.ROCK);
		rdsw=new BlockRdsw(Material.ROCK);
		heq=new BlockHEQ(Material.ROCK);
		bconn=(TexBlock) new TexBlock(Material.ROCK).setUnlocalizedName("bconn").setRegistryName("bconn").setCreativeTab(ctab);
		//maxkabel=new BlockMaxKabel(Material.cloth);
		va=new Item5x5Vent();
		vaB=new ItemFreezer();
		mems=new TexItem[]{(TexItem) 
				(new TexItem()).setUnlocalizedNameB("heatbuffer").setCreativeTab(CreativeTabs.MISC), 
				(TexItem) (new TexItem()).setUnlocalizedNameB("membrancomp").setCreativeTab(CreativeTabs.MISC), 
				(TexItem) (new TexItem()).setUnlocalizedNameB("membranliqheat").setCreativeTab(CreativeTabs.MISC), 
				(TexItem) (new TexItem()).setUnlocalizedNameB("membrantemp").setCreativeTab(CreativeTabs.MISC)};
		owhr=new ItemOWHR();
		owhd=new ItemOWHD();
		owhl=new ItemOWHL();
		owhu=new ItemOWHU();
		coldt=new ItemColdBuffer();
		hott=new ItemHotBuffer();
		fireex=new ItemFireExtinguisher();

		System.out.println("MoarVents: Register Objects");

		ForgeRegistries.ITEMS.register(ctab.tabitem);
		////LanguageRegistry.addName(ctab.tabitem, "Useless CreativeTab Placeholder");
		int bc;

		//va.setCreativeTab(ctab);

		//vaB.setCreativeTab(ctab);


		for (bc=0;bc<mems.length;bc++) {
			mems[bc].setCreativeTab(ctab);
		}

		ForgeRegistries.ITEMS.register(va);
		ForgeRegistries.ITEMS.register(vaB);
		ForgeRegistries.ITEMS.register(mems[0]);
		ForgeRegistries.ITEMS.register(mems[1]);
		ForgeRegistries.ITEMS.register(mems[2]);
		ForgeRegistries.ITEMS.register(mems[3]);
		ForgeRegistries.ITEMS.register(owhr);
		ForgeRegistries.ITEMS.register(owhd);
		ForgeRegistries.ITEMS.register(owhl);
		ForgeRegistries.ITEMS.register(owhu);
		ForgeRegistries.ITEMS.register(coldt);
		ForgeRegistries.ITEMS.register(hott);
		ForgeRegistries.ITEMS.register(fireex);
		ForgeRegistries.BLOCKS.registerAll(cndctr,rhb,shaft,thermo,rdsw,heq,bconn);
		ForgeRegistries.ITEMS.registerAll(
				new ItemBlock(cndctr).setRegistryName("hc").setCreativeTab(ctab),
				new ItemBlock(rhb).setRegistryName("rhb").setCreativeTab(ctab),
				new ItemBlock(shaft).setRegistryName("shaft").setCreativeTab(ctab),
				new ItemBlock(thermo).setRegistryName("thermo").setCreativeTab(ctab),
				new ItemBlock(rdsw).setRegistryName("rdsw").setCreativeTab(ctab),
				new ItemBlock(heq).setRegistryName("heq").setCreativeTab(ctab),
				new ItemBlock(bconn).setRegistryName("bconn").setCreativeTab(ctab)
				);
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(mems[0], 4), new Object[] {Boolean.valueOf(false), new Object[]{"mwm", "wdw", "mwm", 'm', Items.IRON_INGOT, 'w', Items.WATER_BUCKET, 'd', "dustCopper"}}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(mems[1], 4), new Object[] {Boolean.valueOf(false), new Object[]{"m  ", " m ", " rm", 'm', Items.IRON_INGOT, 'r', "itemRubber"}}));
		GameRegistry.addShapedRecipe(new ItemStack(mems[2], 4), new Object[] {"msm", " m ", "   ", 'm', Items.IRON_INGOT, 's', Blocks.SPONGE});
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(mems[2], 4), new Object[] {Boolean.valueOf(false), new Object[]{"msm", " s ", " m ", 'm', Items.IRON_INGOT, 's', "dustStone"}}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(mems[3], 4), new Object[] {Boolean.valueOf(false), new Object[]{"mrm", " w ", "   ", 'm', Items.IRON_INGOT, 'w', Items.WATER_BUCKET, 'r', "itemRubber"}}));
		GameRegistry.addShapedRecipe(new ItemStack(va, 4), new Object[] {"i i", "cb ", "ifi", 'i', Items.IRON_INGOT, 'b', mems[0], 'c', mems[1], 'f', mems[2]});

		GameRegistry.addShapedRecipe(new ItemStack(vaB, 4), new Object[] {"ihi", " bc", "i i", 'i', Items.IRON_INGOT, 'h', mems[3], 'c', mems[1], 'b', mems[0]});

		//LanguageRegistry.addName(mems[0], "Vent Heat Buffer");
		//LanguageRegistry.addName(mems[1], "Component Membran");
		//LanguageRegistry.addName(mems[2], "Dissipation Membran");
		//LanguageRegistry.addName(mems[3], "CoreTemp cooling membran");
		TileEntity.addMapping(TileEntityShaft.class, "mv_shaft");
		TileEntity.addMapping(TileEntityHeatConducter.class, "mv_hc");
		TileEntity.addMapping(TileEntityRHB.class, "mv_rhb");
		TileEntity.addMapping(TileEntityThermo.class, "mv_thermo");
		//TileEntity.addMapping(TEMaxKabel.class, "mv_maxkabel");
		//GameRegistry.registerBlock(cndctr, "mv_hc");
		//LanguageRegistry.addName(cndctr, "Heat Conducter (VERY BUGGY!!!! DONT BUILD CIRCLES!!! (STACKOVERFLOW))");
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(cndctr, 1), new Object[] {Boolean.valueOf(false), new Object[]{"ccc", "sss", "ccc", 'c', "ingotCopper", 's', "ingotSilver"}}));
		//GameRegistry.registerBlock(rhb, "mv_rhb");
		//LanguageRegistry.addName(rhb, "Reactor Heat Bridge");
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(rhb, 1), new Object[] {Boolean.valueOf(false), new Object[]{"ccc", "msm", "ccc", 'm', mems[1], 'c', "ingotCopper", 's', "ingotSilver"}}));
		//GameRegistry.registerBlock(shaft, "mv_shaft");
		//LanguageRegistry.addName(shaft, "Kinetic Shaft");
		GameRegistry.addShapedRecipe(new ItemStack(shaft, 8), new Object[] {" h ", "sss", " h ", 'h', Blocks.STONE, 's', ItemName.crafting.getItemStack(CraftingItemType.iron_shaft)});
		//GameRegistry.registerBlock(thermo, "mv_thermo");
		GameRegistry.addShapedRecipe(new ItemStack(thermo, 4), new Object[] {"iii", "iri", "ibi", 'i', Items.IRON_INGOT, 'r', Items.REDSTONE, 'b', mems[1]});
		//GameRegistry.registerBlock(maxkabel, "maxkabel");
		////LanguageRegistry.instance().addStringLocalization("tile.maxkabel", "de_DE", "MAX KABEL");
		////LanguageRegistry.addName(maxkabel, "MAX CABLE");
		//GameRegistry.addShapedRecipe(new ItemStack(maxkabel, 8), new Object[] {"  a", "   ", "   ", 'a', mems[1]});
		/*GameRegistry.register((ItemBlock) new ItemBlock(cndctr).setRegistryName("mv_hc"));
		GameRegistry.register((ItemBlock) new ItemBlock(rhb).setRegistryName("mv_rhb"));
		GameRegistry.register((ItemBlock) new ItemBlock(shaft).setRegistryName("mv_shaft"));*/
		GameRegistry.addShapedRecipe(new ItemStack(owhr, 2), new Object[] {"   ", " nc", "   ", 'c', mems[1], 'n', Items.GOLD_NUGGET});
		GameRegistry.addShapedRecipe(new ItemStack(owhr, 1), new Object[] {"o", 'o', owhu});
		GameRegistry.addShapedRecipe(new ItemStack(owhd, 1), new Object[] {"o", 'o', owhr});
		GameRegistry.addShapedRecipe(new ItemStack(owhl, 1), new Object[] {"o", 'o', owhd});
		GameRegistry.addShapedRecipe(new ItemStack(owhu, 1), new Object[] {"o", 'o', owhl});
		//LanguageRegistry.addName(owhr, "One Way Heat (R)");
		//LanguageRegistry.addName(owhd, "One Way Heat (D)");
		//LanguageRegistry.addName(owhl, "One Way Heat (L)");
		//LanguageRegistry.addName(owhu, "One Way Heat (U)");
		GameRegistry.addShapedRecipe(new ItemStack(coldt, 4), new Object[] {"mmm", "mim", "mmm", 'm', mems[0], 'i', Items.IRON_INGOT});
		//LanguageRegistry.addName(coldt, "Heat Storage");
		//LanguageRegistry.addName(hott, "Heat Storage (Hot)");
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(bconn, 4), new Object[] {Boolean.valueOf(false), new Object[]{"cmc", "mcm", "cmc", 'c', "blockCopper", 'm', mems[1]}}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(heq, 1), new Object[] {Boolean.valueOf(false), new Object[]{"ccc", "vmv", "ccc", 'c', "blockCopper", 'v', mems[1], 'm', IC2Items.getItem("reactor_heat_vent")}}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(rdsw, 1), new Object[] {Boolean.valueOf(false), new Object[]{"ihi", "imi", " c ", 'c', "circuitBasic", 'i', Items.IRON_INGOT, 'h', Blocks.HOPPER, 'm', ItemName.crafting.getItemStack(CraftingItemType.electric_motor)}}));
		va.setTextureName();
		vaB.setTextureName();
		for(int i=0;i<4;i++) {
			mems[i].setTextureName();
		}
		owhr.setTextureName();
		owhd.setTextureName();
		owhl.setTextureName();
		owhu.setTextureName();
		coldt.setTextureName();
		hott.setTextureName();
		fireex.setTextureName();

		cndctr.registerBlockTexture();
		rhb.registerBlockTexture();
		shaft.registerBlockTexture();
		thermo.registerBlockTexture();
		rdsw.registerBlockTexture();
		heq.registerBlockTexture();
		bconn.registerBlockTexture();
	}
	
	public static SoundEvent sre=new SoundEvent(new ResourceLocation("moarvents","fireex"));
	
	public static IReactor searchForInterface(IBlockAccess w,BlockPos pos,EnumFacing todir){
		TileEntity te=w.getTileEntity(pos);
		if(te instanceof IReactor){return (IReactor)te;}
		else if(te instanceof IReactorChamber)
		{return (IReactor)((IReactorChamber)te).getReactorInstance();}
		else if(te instanceof TileEntityReactorAccessHatch)
		{return (IReactor)((TileEntityReactorAccessHatch)te).getReactorInstance();}// (sic!)
		if(w.getBlockState(pos).getBlock()==bconn) {return searchForInterface(w,pos.add(todir.getDirectionVec()),todir);}
		return null;
	}

	public static TileEntity getTE(IBlockAccess w,BlockPos pos,EnumFacing todir){
		if(w.getBlockState(pos).getBlock()==bconn) {return getTE(w,pos.add(todir.getDirectionVec()),todir);}
		return w.getTileEntity(pos);
	}
}
