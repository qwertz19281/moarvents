package moarvents;

import javax.annotation.Nullable;

import org.lwjgl.input.Keyboard;

import ic2.api.energy.tile.IKineticSource;
import ic2.api.reactor.IReactor;
import ic2.api.reactor.IReactorChamber;
import ic2.core.block.reactor.tileentity.TileEntityReactorAccessHatch;
import net.minecraft.block.BlockRedstoneWire;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class TileEntityThermo extends TileEntity implements ITickable
{
	public int offTemp=1, onTemp=1, currTemp=-42;
	public boolean redstone=false,fupd=false,redinv=false;
	public BlockPos down=null,up=null,east=null,south=null,west=null,north=null;
	@Override
	public void update() {
		if(worldObj==null||worldObj.isRemote){return;}
		if(fupd){IBlockState bs=worldObj.getBlockState(pos);worldObj.notifyNeighborsOfStateChange(pos, MoarVents.thermo);worldObj.notifyBlockUpdate(pos,bs,bs,2);fupd=false;}
		if(down==null){
			down=new BlockPos(pos.getX(),pos.getY()-1,pos.getZ());
			up=new BlockPos(pos.getX(),pos.getY()+1,pos.getZ());
			east=new BlockPos(pos.getX()+1,pos.getY(),pos.getZ());
			south=new BlockPos(pos.getX(),pos.getY(),pos.getZ()+1);
			west=new BlockPos(pos.getX()-1,pos.getY(),pos.getZ());
			north=new BlockPos(pos.getX(),pos.getY(),pos.getZ()-1);
		}
		IReactor d=MoarVents.searchForInterface(worldObj,down,EnumFacing.DOWN);
		IReactor u=MoarVents.searchForInterface(worldObj,up,EnumFacing.UP);
		IReactor e=MoarVents.searchForInterface(worldObj,east,EnumFacing.EAST);
		IReactor s=MoarVents.searchForInterface(worldObj,south,EnumFacing.SOUTH);
		IReactor w=MoarVents.searchForInterface(worldObj,west,EnumFacing.WEST);
		IReactor n=MoarVents.searchForInterface(worldObj,north,EnumFacing.NORTH);
		currTemp=-20000;
		currTemp=Math.max(currTemp, (d==null)?-20000:d.getHeat());
		currTemp=Math.max(currTemp, (u==null)?-20000:u.getHeat());
		currTemp=Math.max(currTemp, (e==null)?-20000:e.getHeat());
		currTemp=Math.max(currTemp, (s==null)?-20000:s.getHeat());
		currTemp=Math.max(currTemp, (w==null)?-20000:w.getHeat());
		currTemp=Math.max(currTemp, (n==null)?-20000:n.getHeat());
		if(currTemp<-19999){return;}
		//IBlockState bs=worldObj.getBlockState(pos);
		//Integer p=bs.getValue(BlockRedstoneWire.POWER);
		/*if(p==null){
			worldObj.setBlockState(pos, bs.withProperty(BlockRedstoneWire.POWER, redstone?15:0), 2);
			worldObj.notifyNeighborsOfStateChange(pos, MoarVents.thermo);
			bs=worldObj.getBlockState(pos);
			p=bs.getValue(BlockRedstoneWire.POWER);
			if(d==null){worldObj.notifyNeighborsOfStateChange(down, MoarVents.thermo);}
			if(u==null){worldObj.notifyNeighborsOfStateChange(up, MoarVents.thermo);}
			if(e==null){worldObj.notifyNeighborsOfStateChange(east, MoarVents.thermo);}
			if(s==null){worldObj.notifyNeighborsOfStateChange(south, MoarVents.thermo);}
			if(w==null){worldObj.notifyNeighborsOfStateChange(west, MoarVents.thermo);}
			if(n==null){worldObj.notifyNeighborsOfStateChange(north, MoarVents.thermo);}
		}*/
		if(redstone){
			if(currTemp<offTemp){
				redstone=false;
				IBlockState bs=worldObj.getBlockState(pos);worldObj.notifyNeighborsOfStateChange(pos, MoarVents.thermo);worldObj.notifyBlockUpdate(pos,bs,bs,2);
			}
		} else {
			if(currTemp>=onTemp){
				redstone=true;
				IBlockState bs=worldObj.getBlockState(pos);worldObj.notifyNeighborsOfStateChange(pos, MoarVents.thermo);worldObj.notifyBlockUpdate(pos,bs,bs,2);
			}
		}
		//System.out.println("c="+currTemp+" of="+offTemp+" on="+onTemp+" i="+(redinv?"t":"f")+" rs="+(redstone?"t":"f"));
		/*if(redstone!=(p.intValue()>7)){
			worldObj.setBlockState(pos, bs.withProperty(BlockRedstoneWire.POWER, redstone?15:0), 2);
			worldObj.notifyNeighborsOfStateChange(pos, MoarVents.thermo);
			if(d==null){worldObj.notifyNeighborsOfStateChange(down, MoarVents.thermo);}
			if(u==null){worldObj.notifyNeighborsOfStateChange(up, MoarVents.thermo);}
			if(e==null){worldObj.notifyNeighborsOfStateChange(east, MoarVents.thermo);}
			if(s==null){worldObj.notifyNeighborsOfStateChange(south, MoarVents.thermo);}
			if(w==null){worldObj.notifyNeighborsOfStateChange(west, MoarVents.thermo);}
			if(n==null){worldObj.notifyNeighborsOfStateChange(north, MoarVents.thermo);}
		}*/
	}
	
	@Override
	public void readFromNBT(NBTTagCompound compound)
	{
		super.readFromNBT(compound);
		//System.out.println("READ");
		if(worldObj!=null&&worldObj.isRemote){return;}
		if(compound.hasKey("offTemp")){offTemp=compound.getInteger("offTemp");}
		if(compound.hasKey("onTemp")){onTemp=compound.getInteger("onTemp");}
		if(compound.hasKey("redinv")){redinv=compound.getBoolean("redinv");}
		fupd=true;
		try{update();}catch(Exception ex){}
		try{IBlockState bs=worldObj.getBlockState(pos);worldObj.notifyNeighborsOfStateChange(pos, MoarVents.thermo);worldObj.notifyBlockUpdate(pos,bs,bs,2);}catch(Exception ex){}
	}
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound)
	{
		super.writeToNBT(compound);
		//System.out.println("WRITE");
		if(worldObj!=null&&worldObj.isRemote){return compound;}
		compound.setInteger("offTemp",offTemp);
		compound.setInteger("onTemp",onTemp);
		compound.setBoolean("redinv",redinv);
		return compound;
	}
	
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ){
		if(worldObj!=null&&worldObj.isRemote){return true;}
		//System.out.println(side.getName()+"	"+(int)(hitX*2)+"	"+(int)(hitY*2)+"	"+(int)(hitZ*2));
		/*if(Keyboard.isKeyDown(Keyboard.KEY_UP)||Keyboard.isKeyDown(Keyboard.KEY_ADD)){
			if(Keyboard.isKeyDown(Keyboard.KEY_LEFT)||Keyboard.isKeyDown(Keyboard.KEY_MULTIPLY)){
				offTemp++;
			} else {
				onTemp++;
			}
		}else if(Keyboard.isKeyDown(Keyboard.KEY_DOWN)||Keyboard.isKeyDown(Keyboard.KEY_MINUS)){
			if(Keyboard.isKeyDown(Keyboard.KEY_LEFT)||Keyboard.isKeyDown(Keyboard.KEY_MULTIPLY)){
				offTemp--;
			} else {
				onTemp--;
			}
		}else if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT)||Keyboard.isKeyDown(Keyboard.KEY_DIVIDE)){
			redinv=!redinv;
		}*/
		//System.out.println(playerIn.getHeldItemMainhand());
		if(playerIn.isSneaking()&&(playerIn.getHeldItemMainhand()==null)) {
			int tmode=touchcode[((int)(transTouchY(side,hitX,hitY,hitZ)*16))*16+((int)(transTouchX(side,hitX,hitY,hitZ)*16))];
			//System.out.println(side.getName()+"	"+(int)(hitX*2)+"	"+(int)(hitY*2)+"	"+(int)(hitZ*2));
			//System.out.println(transTouchX(side,hitX,hitY,hitZ)+"	"+transTouchY(side,hitX,hitY,hitZ));
			switch(tmode) {
				case 1: offTemp++; break;
				case 2: offTemp--; break;
				case 3: onTemp++; break;
				case 4: onTemp--; break;
				case 6: offTemp+=25; break;
				case 7: offTemp-=25; break;
				case 8: onTemp+=25; break;
				case 9: onTemp-=25; break;
				case 5: redinv=!redinv; break;
				default: break;
			}
		}
		if(offTemp<0){offTemp=0;}if(onTemp<0){onTemp=0;}
		playerIn.addChatMessage(new TextComponentString("offTemp="+offTemp+" onTemp="+onTemp+" invert="+redinv));
		IBlockState bs=worldObj.getBlockState(pos);worldObj.notifyNeighborsOfStateChange(pos, MoarVents.thermo);worldObj.notifyBlockUpdate(pos,bs,bs,2);
		return true;
	}
	
	public static float transTouchX(EnumFacing s,float x,float y,float z) {
		switch(s.getIndex()) {
			case 0: return x;
			case 1: return x;
			case 2: return 1-x;
			case 3: return x;
			case 4: return z;
			case 5: return 1-z;
			default: return 0;
		}
	}
	public static float transTouchY(EnumFacing s,float x,float y,float z) {
		switch(s.getIndex()) {
			case 0: return 1-z;
			case 1: return z;
			case 2: return 1-y;
			case 3: return 1-y;
			case 4: return 1-y;
			case 5: return 1-y;
			default: return 0;
		}
	}
	
	public static short[] touchcode=new short[] {
		0,0,1,0,0,6,0,0,0,0,8,0,0,3,0,0,
		0,1,1,1,6,6,6,0,0,8,8,8,3,3,3,0,
		1,1,1,1,1,6,0,0,0,0,8,3,3,3,3,3,
		0,1,1,1,0,0,0,0,0,0,0,0,3,3,3,0,
		0,0,1,0,0,0,0,0,0,0,0,0,0,3,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		2,2,2,2,7,7,7,0,0,9,9,9,4,4,4,4,
		2,2,2,2,2,0,5,5,5,5,0,4,4,4,4,4,
		2,2,2,2,2,0,5,5,5,5,0,4,4,4,4,4,
		0,0,0,0,0,0,5,5,5,5,0,0,0,0,0,0
	};
}
