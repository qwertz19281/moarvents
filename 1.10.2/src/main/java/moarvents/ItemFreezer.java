package moarvents;

import ic2.api.reactor.IReactor;
import ic2.api.reactor.IReactorComponent;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemFreezer extends TexItem implements IReactorComponent {

	public ItemFreezer()
	{
		this.setMaxStackSize(64);
		this.setCreativeTab(MoarVents.ctab);
		this.setUnlocalizedNameB("venthullcooler");
		//this.setTextureName();
	}
	
	@Override
	public void processChamber(ItemStack yourStack, IReactor reactor, int x, int y, boolean heatrun) {
		if(heatrun){
			int toMove=Math.min(128,reactor.getHeat());
			int heat=toMove;
			//System.out.print(heat+" ");
			heat=alterNeighborHeat(reactor,x+1,y,heat);
			//System.out.print(heat+" ");
			heat=alterNeighborHeat(reactor,x,y+1,heat);
			//System.out.print(heat+" ");
			heat=alterNeighborHeat(reactor,x-1,y,heat);
			//System.out.print(heat+" ");
			heat=alterNeighborHeat(reactor,x,y-1,heat);
			//System.out.print(heat+"\n");
			reactor.addHeat(heat-toMove);
		}
	}

	@Override
	public boolean acceptUraniumPulse(ItemStack yourStack, IReactor reactor, ItemStack pulsingStack, int youX, int youY,
			int pulseX, int pulseY, boolean heatrun) {
		return false;
	}

	@Override
	public boolean canStoreHeat(ItemStack yourStack, IReactor reactor, int x, int y) {
		return false;
	}

	@Override
	public int getMaxHeat(ItemStack yourStack, IReactor reactor, int x, int y) {
		return 0;
	}

	@Override
	public int getCurrentHeat(ItemStack yourStack, IReactor reactor, int x, int y) {
		return 0;
	}

	@Override
	public int alterHeat(ItemStack yourStack, IReactor reactor, int x, int y, int heat) {
		/*int haet=heat;
		if(haet>=0){return haet;}
		haet=Math.max(haet,-reactor.getHeat());
		haet=Math.max(haet,);
		reactor.addHeat(-haet);
		return haet+heat;*/
		return heat;
	}

	@Override
	public float influenceExplosion(ItemStack yourStack, IReactor reactor) {
		return 0;
	}

	public int alterNeighborHeat(IReactor reactor,int x,int y,int heat){
		ItemStack is=reactor.getItemAt(x, y);
		if(is==null){return heat;}
		return ((IReactorComponent)is.getItem()).alterHeat(reactor.getItemAt(x,y), reactor, x,y, heat);
	}

	@Override
	public boolean canBePlacedIn(ItemStack arg0, IReactor arg1) {
		// TODO Auto-generated method stub
		return true;
	}
}
