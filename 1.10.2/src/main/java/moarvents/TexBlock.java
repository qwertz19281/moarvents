package moarvents;

import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;

public class TexBlock extends Block {

	public TexBlock(Material materialIn) {
		super(materialIn);
	}

	public TexBlock(Material materialIn, MapColor mc) {
		super(materialIn,mc);
	}
	
	public Block registerBlockTexture() {
		if(FMLCommonHandler.instance().getSide()==Side.CLIENT){
			String texName="moarvents:"+(getUnlocalizedName().substring(5));
			System.out.println("Load Block Tex: "+texName);
			Minecraft
            .getMinecraft()
            .getRenderItem()
            .getItemModelMesher()
            .register(Item.getItemFromBlock(this), 0,
                new ModelResourceLocation(texName));
        }
		return this;
	}
}
