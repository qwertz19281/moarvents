package moarvents;

import javax.annotation.Nullable;

import ic2.api.reactor.IReactor;
import ic2.api.reactor.IReactorChamber;
import ic2.core.block.reactor.tileentity.TileEntityReactorAccessHatch;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRedstoneWire;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockThermo extends TexBlock implements ITileEntityProvider
{
	public BlockThermo(Material m)
	{
		super(m);
		this.setUnlocalizedName("thermo");
		this.setRegistryName("thermo");
		//this.setBlockTextureName();
		this.setCreativeTab(MoarVents.ctab);
	}

	/**
	 * Returns a new instance of a block's tile entity class. Called on placing the block.
	 */
	@Override
	public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_)
	{
		return new TileEntityThermo();
	}
	@Override
	public boolean canProvidePower(IBlockState state) {
	    return true;
	}
	
	@Override
	public int getWeakPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
    {
		//if(blockAccess.){return 0;}
		//System.out.println("wp");
		TileEntityThermo te=(TileEntityThermo)blockAccess.getTileEntity(pos);
		IReactor wiaekdor=MoarVents.searchForInterface(blockAccess,pos.add(side.getOpposite().getDirectionVec()),side.getOpposite());
		//if(wiaekdor!=null){System.out.println("ItZ a wiaekdor side="+side);},
		return ((te.redinv?(!te.redstone):te.redstone)&&wiaekdor==null)?15:0;
	}
	
	@Override
    public int getStrongPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
    {
		/*System.out.println("sp");
		TileEntityThermo te=(TileEntityThermo)blockAccess.getTileEntity(pos);
		return te.redstone?15:0;*/
		return getWeakPower(blockState,blockAccess,pos,side);
    }
	@Override
    public boolean shouldCheckWeakPower(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing side)
    {
        return false;
    }
	
	public IReactor searchForInterface(TileEntity te){
		if(te instanceof IReactor){return (IReactor)te;}
		else if(te instanceof IReactorChamber)
		{return (IReactor)((IReactorChamber)te).getReactorInstance();}
		else if(te instanceof TileEntityReactorAccessHatch)
		{return (IReactor)((TileEntityReactorAccessHatch)te).getReactorInstance();}// (sic!)
		return null;
	}
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, ItemStack is, EnumFacing side, float hitX, float hitY, float hitZ)
    {
		if(worldIn!=null&&worldIn.isRemote){return true;}
        return ((TileEntityThermo)worldIn.getTileEntity(pos)).onBlockActivated(worldIn,pos,state,playerIn,hand,side,hitX,hitY,hitZ);
    }
}
