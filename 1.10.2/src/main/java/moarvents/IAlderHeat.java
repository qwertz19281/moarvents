package moarvents;

import java.util.ArrayList;

import ic2.api.reactor.IReactor;
import net.minecraft.item.ItemStack;

public interface IAlderHeat {
	public int alderHeat(IReactor reactor, ItemStack yourStack, int x, int y, int heat, ArrayList<IAlderHeat> arhc);
}
