package moarvents;

import java.util.ArrayList;

import ic2.api.energy.tile.IHeatSource;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;

public class TileEntityHeatConducter extends TileEntity implements IHeatSource
{
	BlockPos[] aa=null,ab=null;
	public void createTbls() {
		aa=new BlockPos[] {
				new BlockPos(pos.getX(),pos.getY()-1,pos.getZ()),
				new BlockPos(pos.getX(),pos.getY()+1,pos.getZ()),
				new BlockPos(pos.getX(),pos.getY(),pos.getZ()-1),
				new BlockPos(pos.getX(),pos.getY(),pos.getZ()+1),
				new BlockPos(pos.getX()-1,pos.getY(),pos.getZ()),
				new BlockPos(pos.getX()+1,pos.getY(),pos.getZ())
		};
		ab=new BlockPos[] {
				new BlockPos(pos.getX(),pos.getY()-2,pos.getZ()),
				new BlockPos(pos.getX(),pos.getY()+2,pos.getZ()),
				new BlockPos(pos.getX(),pos.getY(),pos.getZ()-2),
				new BlockPos(pos.getX(),pos.getY(),pos.getZ()+2),
				new BlockPos(pos.getX()-2,pos.getY(),pos.getZ()),
				new BlockPos(pos.getX()+2,pos.getY(),pos.getZ())
		};
	}
	@Override
	public int maxrequestHeatTick(EnumFacing directionFrom)
	{
		if(aa==null) {createTbls();}
		int voa = 0;

		
		ArrayList<TileEntityHeatConducter> begin=new ArrayList<TileEntityHeatConducter>();
		begin.add(this);
		
		for (int ax = 0; ax < 6; ++ax)
		{
			EnumFacing fgd = EnumFacing.VALUES[ax];

			if (!fgd.equals(directionFrom))
			{
				TileEntity te = this.worldObj.getTileEntity((worldObj.getBlockState(aa[ax]).getBlock()==MoarVents.bconn)?ab[ax]:aa[ax]);
				if (te != null)
				{
					if (te instanceof TileEntityHeatConducter)
					{
						
						voa += ((TileEntityHeatConducter)te).maxrequestHeatTick(fgd.getOpposite(),begin);
						
					}
					else if (te instanceof IHeatSource)
					{
						voa += ((IHeatSource)te).maxrequestHeatTick(fgd.getOpposite());
					}
				}
			}
		}

		begin.clear();
		
		return voa;
	}

	public int maxrequestHeatTick(EnumFacing directionFrom, ArrayList<TileEntityHeatConducter> arhc)
	{
		if(aa==null) {createTbls();}
		if (arhc.contains(this))
		{
			return 0;
		}
		else
		{
			arhc.add(this);
			int voa = 0;

			for (int ax = 0; ax < 6; ++ax)
			{
				EnumFacing fgd = EnumFacing.VALUES[ax];

				if (!fgd.equals(directionFrom))
				{
					TileEntity te = this.worldObj.getTileEntity((worldObj.getBlockState(aa[ax]).getBlock()==MoarVents.bconn)?ab[ax]:aa[ax]);
					if (te != null)
					{
						if (te instanceof TileEntityHeatConducter)
						{
							voa += ((TileEntityHeatConducter)te).maxrequestHeatTick(fgd.getOpposite(), arhc);
						}
						else if (te instanceof IHeatSource)
						{
							voa += ((IHeatSource)te).maxrequestHeatTick(fgd.getOpposite());
						}
					}
				}
			}

			return voa;
		}
	}

	@Override
	public int requestHeat(EnumFacing directionFrom, int requestheat)
	{
		if(aa==null) {createTbls();}
		int voa = requestheat;

		ArrayList<TileEntityHeatConducter> begin=new ArrayList<TileEntityHeatConducter>();
		begin.add(this);
		
		for (int ax = 0; ax < 6; ++ax)
		{
			EnumFacing fgd = EnumFacing.VALUES[ax];

			if (!fgd.equals(directionFrom))
			{
				TileEntity te = this.worldObj.getTileEntity((worldObj.getBlockState(aa[ax]).getBlock()==MoarVents.bconn)?ab[ax]:aa[ax]);
				if (te != null)
				{
					if (te instanceof TileEntityHeatConducter)
					{
						
						voa -= ((TileEntityHeatConducter)te).requestHeat(fgd.getOpposite(), voa, begin);
						
					}
					else if (te instanceof IHeatSource)
					{
						voa -= ((IHeatSource)te).requestHeat(fgd.getOpposite(), voa);
					}
				}
			}
		}
		
		begin.clear();

		return requestheat - voa;
	}

	public int requestHeat(EnumFacing directionFrom, int requestheat, ArrayList<TileEntityHeatConducter> arhc)
	{
		if(aa==null) {createTbls();}
		if (arhc.contains(this))
		{
			return 0;
		}
		else
		{
			arhc.add(this);
			int voa = requestheat;

			for (int ax = 0; ax < 6; ++ax)
			{
				EnumFacing fgd = EnumFacing.VALUES[ax];

				if (!fgd.equals(directionFrom))
				{
					TileEntity te = this.worldObj.getTileEntity((worldObj.getBlockState(aa[ax]).getBlock()==MoarVents.bconn)?ab[ax]:aa[ax]);
					if (te != null)
					{
						if (te instanceof TileEntityHeatConducter)
						{
							voa -= ((TileEntityHeatConducter)te).requestHeat(fgd.getOpposite(), voa, arhc);
						}
						else if (te instanceof IHeatSource)
						{
							voa -= ((IHeatSource)te).requestHeat(fgd.getOpposite(), voa);
						}
					}
				}
			}

			return requestheat - voa;
		}
	}
	
	/*@Override
	public int drawHeat(EnumFacing directionFrom, int requestheat, boolean sim)
	{
		if(aa==null) {createTbls();}
		int voa = requestheat;

		ArrayList<TileEntityHeatConducter> begin=new ArrayList<TileEntityHeatConducter>();
		begin.add(this);
		
		for (int ax = 0; ax < 6; ++ax)
		{
			EnumFacing fgd = EnumFacing.VALUES[ax];

			if (!fgd.equals(directionFrom))
			{
				TileEntity te = this.worldObj.getTileEntity((worldObj.getBlockState(aa[ax]).getBlock()==MoarVents.bconn)?ab[ax]:aa[ax]);
				if (te != null)
				{
					if (te instanceof TileEntityHeatConducter)
					{
						
						voa -= ((TileEntityHeatConducter)te).drawHeat(fgd.getOpposite(), voa, begin, sim);
						
					}
					else if (te instanceof IHeatSource)
					{
						voa -= ((IHeatSource)te).drawHeat(fgd.getOpposite(), voa, sim);
					}
				}
			}
		}
		
		begin.clear();

		return requestheat - voa;
	}

	public int drawHeat(EnumFacing directionFrom, int requestheat, ArrayList<TileEntityHeatConducter> arhc, boolean sim)
	{
		if(aa==null) {createTbls();}
		if (arhc.contains(this))
		{
			return 0;
		}
		else
		{
			arhc.add(this);
			int voa = requestheat;

			for (int ax = 0; ax < 6; ++ax)
			{
				EnumFacing fgd = EnumFacing.VALUES[ax];

				if (!fgd.equals(directionFrom))
				{
					TileEntity te = this.worldObj.getTileEntity((worldObj.getBlockState(aa[ax]).getBlock()==MoarVents.bconn)?ab[ax]:aa[ax]);
					if (te != null)
					{
						if (te instanceof TileEntityHeatConducter)
						{
							voa -= ((TileEntityHeatConducter)te).drawHeat(fgd.getOpposite(), voa, arhc, sim);
						}
						else if (te instanceof IHeatSource)
						{
							voa -= ((IHeatSource)te).drawHeat(fgd.getOpposite(), voa, sim);
						}
					}
				}
			}

			return requestheat - voa;
		}
	}*/
}
