package moarvents;

import ic2.api.reactor.IReactor;
import ic2.api.reactor.IReactorChamber;
import ic2.core.block.reactor.tileentity.TileEntityReactorAccessHatch;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;

public class TileEntityHEQ extends TileEntity implements ITickable {
	public BlockPos down=null,up=null,east=null,south=null,west=null,north=null;
	IReactor[] r=new IReactor[6];
	@Override
	public void update() {
		if(worldObj==null||worldObj.isRemote){return;}
		if(down==null){
			down=new BlockPos(pos.getX(),pos.getY()-1,pos.getZ());
			up=new BlockPos(pos.getX(),pos.getY()+1,pos.getZ());
			east=new BlockPos(pos.getX()+1,pos.getY(),pos.getZ());
			south=new BlockPos(pos.getX(),pos.getY(),pos.getZ()+1);
			west=new BlockPos(pos.getX()-1,pos.getY(),pos.getZ());
			north=new BlockPos(pos.getX(),pos.getY(),pos.getZ()-1);
		}
		r[5]=MoarVents.searchForInterface(worldObj,down,EnumFacing.DOWN);
		r[0]=MoarVents.searchForInterface(worldObj,up,EnumFacing.UP);
		r[1]=MoarVents.searchForInterface(worldObj,east,EnumFacing.EAST);
		r[2]=MoarVents.searchForInterface(worldObj,south,EnumFacing.SOUTH);
		r[3]=MoarVents.searchForInterface(worldObj,west,EnumFacing.WEST);
		r[4]=MoarVents.searchForInterface(worldObj,north,EnumFacing.NORTH);
		int h=0,c=0;
		for(int i=0;i<6;i++) {
			if(r[i]!=null) {
				c++;
				h+=r[i].getHeat();
			}
		}
		if(c<1) {return;}
		int hc=h/c;
		int rest=h-(hc*c);
		for(int i=0;i<6;i++) {
			if(r[i]!=null) {
				r[i].setHeat(hc);
			}
		}
		if(rest<0) {System.err.println("M����P");}
		int dctr=5;
		while(rest>0) {
			dctr++;if(dctr>5) {dctr-=6;}
			if(r[dctr]!=null) {
				r[dctr].addHeat(1);
				rest--;
			}
		}
	}
	
}
