package moarvents;

import java.util.ArrayList;

import ic2.api.reactor.IReactor;
import ic2.api.reactor.IReactorComponent;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemOWHU extends TexItem implements IReactorComponent,IAlderHeat {

	public ItemOWHU()
	{
		this.setMaxStackSize(1);
		this.setCreativeTab(MoarVents.ctab);
		this.setUnlocalizedNameB("ventowhu");
		//this.setTextureName();
	}
	
	boolean evilbaum=true;
	

	@Override
	public int alterHeat(ItemStack yourStack, IReactor reactor, int x, int y, int heat) {
		int haet=heat;
		if(haet<1){return haet;}
		ArrayList<IAlderHeat> arhc=new ArrayList<IAlderHeat>();
		arhc.add(this);
		haet=alterNeighborHeat(reactor,x,y-1,haet,arhc);
		arhc.clear();
		return haet;
	}
	
	@Override
	public int alderHeat(IReactor reactor, ItemStack yourStack, int x, int y, int heat, ArrayList<IAlderHeat> arhc) {
		int haet=heat;
		if(arhc.contains(this)){if(evilbaum){System.err.println("[MoarVents] at 37 OWHD");evilbaum=false;}return haet;}
		arhc.add(this);
		if(haet<1){return haet;}
		haet=alterNeighborHeat(reactor,x,y-1,haet,arhc);
		return haet;
	}

	public int alterNeighborHeat(IReactor reactor,int x,int y,int heat,ArrayList<IAlderHeat> arhc){
		ItemStack is=reactor.getItemAt(x, y);
		if(is==null){return heat;}
		IReactorComponent rc=((IReactorComponent)is.getItem());
		if(rc instanceof IAlderHeat){
			return ((IAlderHeat)rc).alderHeat(reactor, reactor.getItemAt(x, y), x, y, heat,arhc);
		} else {	
			return rc.alterHeat(reactor.getItemAt(x,y), reactor, x,y, heat);
		}
	}


	@Override
	public boolean canBePlacedIn(ItemStack arg0, IReactor arg1) {
		// TODO Auto-generated method stub
		return true;
	}


	@Override
	public boolean acceptUraniumPulse(ItemStack arg0, IReactor arg1, ItemStack arg2, int arg3, int arg4, int arg5,
			int arg6, boolean arg7) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean canStoreHeat(ItemStack arg0, IReactor arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public int getCurrentHeat(ItemStack arg0, IReactor arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public int getMaxHeat(ItemStack arg0, IReactor arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public float influenceExplosion(ItemStack arg0, IReactor arg1) {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void processChamber(ItemStack arg0, IReactor arg1, int arg2, int arg3, boolean arg4) {
		// TODO Auto-generated method stub
		
	}
}
